// util.c - generic functions for GPMON 
// Compile code with
// gcc -o gpmon  gpmon.c xrf.c util.c -l wiringPi -l m -l pthread -l curl
// to debug add -g -rdynamic
// ThingSPeak API Write KEY 8GL51FPSEAAHQ2GL for channel 7091 and LYJWX4G395JICTO0 for 19111
#include "gpmon.h"
#include <curl/curl.h>
#include <dirent.h>
#include <stdarg.h>
#include <string.h>

// for i2c channel
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>


 
#define DELTASEC 300 // maximum age of a file to be sent via email
#define MIMELEN	76
#define MIMEEND "\n"
typedef struct
{
	int linea;
	char **data;
} CURLBUF;

char* logFN;
FILE* logF;
int logOn = 0; // general log level 0 none, 1 error, 2 info, 3 debug
int logOnI = 0; // IRDA functions log level 0 none, 1 error, 2 info, 3 debug
int logOnX = 0; // logging enabled for XRF module
extern struct CFGDATA cfg; // global variables


CURL*  curl;
CURLcode res;
static const char *I2Cbus = "/dev/i2c-1";	// I2C bus
pthread_mutex_t thingSpeakCall = PTHREAD_MUTEX_INITIALIZER; // mutex for thingSpeak publish fnction
pthread_mutex_t curlMTX = PTHREAD_MUTEX_INITIALIZER; // mutex for curl usage
pthread_mutex_t _loggerMTX = PTHREAD_MUTEX_INITIALIZER; // mutex for logging
pthread_mutex_t i2cMTX = PTHREAD_MUTEX_INITIALIZER; // mutex for i2c channel

// connect to I2C bus and query device with nwr bytes from wr and put nrd bytes in rd
// do not use SMBUS
bool I2Ccomm(int dev, unsigned char* wr, int nwr, unsigned char* rd, int nrd) {
int fd = 0;
bool OK = 0;

	logger(3, "I2Ccomm entering device 0x%X wr=%d rd=%d", dev, nwr, nrd);
	if(pthread_mutex_lock(&i2cMTX)) {
		logger(0, "I2CComm lock MTX: %s", strerror(errno));
		return 0;
	}
	
	if((fd=open(I2Cbus, O_RDWR)) > 0) {
		if(ioctl(fd, I2C_SLAVE, dev) == 0) {

			if(nwr > 0 && write(fd, wr, nwr) < 0)
				logger(0, "I2Ccomm Error writing request %d byte(s): %s", nwr, strerror(errno));
			else if(nrd > 0 && read(fd, rd, nrd) < 0)
				logger(0, "I2Ccomm Error reading request %d byte(s): %s", nrd, strerror(errno));
			else 
				OK = 1;
		}
		else
			logger(0, "I2Ccomm Device 0x%X failed connect: %s", dev, strerror(errno));

		close(fd);
	}
	else 
		logger(0, "I2Ccomm Error opening %s bus: %s", I2Cbus, strerror(errno));

	logger(3, "I2Ccomm exiting OK=%d", OK);
	if(pthread_mutex_unlock(&i2cMTX))
		logger(0, "I2CComm unlock MTX:%s", strerror(errno));
	if(OK) errno = 0;
	return OK;
}

// if read is 1 return the value readed from register, if read is 0 put on value in register
// in case of write return 0 if failue or 1 if OK; bit is from 1 to 8
bool MCP23017(int port, bool read, int bit, bool on) {
register unsigned char mask;
unsigned char wr[4], rd[4];


	logger(3, "MCP23017 entering bit=%d on=%d", bit,on);
	memset(wr, 0, sizeof(wr));
	memset(rd, 0, sizeof(rd));
	mask = (1 << (bit-1));
	logger(4, "Mask %s", byte2bin(mask));
	wr[0] = port;
	if(I2Ccomm(0x20, wr, 1, rd, 1)) {
		logger(4, "Read %s", byte2bin(rd[0]));
		if(read) {
			logger(3, "MCP23017 ris=%d", rd[0] & mask);
			return (rd[0] & mask) != 0;
		}
		wr[0] = port;
		wr[1] = (rd[0] & ~mask) | (on ? mask : 0x00);
		logger(4, "Writing %s ", byte2bin(rd[0] & ~mask));
		logger(4, "Writing %s ", byte2bin(on ? mask : 0x00));
		logger(4, "Writing %s", byte2bin(wr[1]));
		memset(rd, 0, sizeof(rd));
		if(I2Ccomm(0x20, wr, 2, rd, 0)) {
			logger(3, "MCP23017 ris=1");
			return 1;
		}
	}
	logger(3, "MCP23017 ris=0");
	return 0;
}

/*
** encodeblock
**
** encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
static inline void encodeblock( unsigned char *in, unsigned char *out, int len )
{
static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    out[0] = (unsigned char) cb64[ (int)(in[0] >> 2) ];
    out[1] = (unsigned char) cb64[ (int)(((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)) ];
    out[2] = (unsigned char) (len > 1 ? cb64[ (int)(((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)) ] : '=');
    out[3] = (unsigned char) (len > 2 ? cb64[ (int)(in[2] & 0x3f) ] : '=');
}

int mimeHeader(char* dst, int size, const char* to, const char* from, char* subject, int reset) {
static char* linee[] = {
	"From: ALLARMI <%s>",
	"To: ME <%s>",
	"Subject: titolo mail",
	"MIME-Version: 1.0",
	"Content-Type: multipart/mixed; boundary=pj+EhsWuS_QJx.x7p_r",
	"Date: ",
	"",
	0 };
static int pnt = 0;
int len = 0;

	if(reset)
		pnt = 0;

	if(!linee[pnt])
		len = 0;
	else if(strstr(linee[pnt], "From:")) {
		len = snprintf(dst, size, "From: ALLARMI <%s>%s", from, MIMEEND);
	}
	else if(strstr(linee[pnt], "To:")) {
		len = snprintf(dst, size, "To: ME <%s>%s", to, MIMEEND);
	}
	else if(strstr(linee[pnt], "Subject:")) {
		len = snprintf(dst, size, "Subject: %s%s", subject, MIMEEND);
	}
	else if(strstr(linee[pnt], "Date:")) {
	time_t ora = time(NULL);
	struct tm lt;

		ora = time(NULL);
		localtime_r(&ora, &lt);
		len = strftime(dst, size, "Date: %a, %d %b %Y %T %z" MIMEEND, &lt);
	}
	else {
		len = snprintf(dst, size, "%s%s", linee[pnt], MIMEEND);
	}
	if(len > 0)
		pnt++;
	return len;
}
int mimeTesto(char* dst, int size, char** testo, int reset) {
static char* linee[] = {
	"--pj+EhsWuS_QJx.x7p_r",
	"Content-Type: text/plain; charset=\"us-ascii\"",
	"Content-Transfer-Encoding: quoted-printable",
	"",
	"TESTO",
	"",
	0 };
static int pnt = 0, pnt2 = 0;
int len = 0;

	if(reset) {
		pnt = pnt2 = 0;
	}
	
	if(!linee[pnt])
		len = 0;
	else if(strstr(linee[pnt], "TESTO")) {
		if(testo[pnt2])
			len = snprintf(dst, size, "%s%s", testo[pnt2++], MIMEEND);

		if(testo[pnt2] == NULL) {
			pnt++;
			pnt2 = 0;
		}
	}
	else {
		len = snprintf(dst, size, "%s%s", linee[pnt++], MIMEEND);
	}
	return len;
}

int mimeAttach(char* dst, int size, char* nome, int reset)
{
static char* linee[] = {
	"--pj+EhsWuS_QJx.x7p_r",
	"Content-Type: application/octet-stream; name=\"",
	"Content-Transfer-Encoding: base64",
	"Content-Disposition: attachment; filename=\"",
	"",
	"FILE",
	"",
	0 };
static FILE* pf = NULL;
static int pnt = 0;
int len = 0;

	if(reset)
		pnt = 0;
	
	if(!linee[pnt])
		len = 0;
	else if(strstr(linee[pnt], "FILE") && nome) {
	unsigned char in[3];
	register int i, num, blocksout = 0;
	register unsigned char* pt;

		if(pf == NULL && (pf = fopen(nome, "rb")) == NULL)
			return -1;

		*in = (unsigned char) 0;
		pt = (unsigned char*)dst;
		*pt = (unsigned char) 0;
		while( feof( pf ) == 0 ) {
			num = 0;
			for(i = 0; i < 3; i++) {
				in[i] = (unsigned char) getc( pf );

				if( feof( pf ) == 0 ) {
					num++;
				}
				else {
					in[i] = (unsigned char) 0;
				}
			}
			if( num > 0 ) {
				encodeblock( in, pt, num );
				blocksout++;
			}
			pt += 4;
			if( blocksout >= (MIMELEN/4) || feof( pf ) != 0 ) {
				if( blocksout > 0 ) {
					strcpy( (char*)pt, MIMEEND );
				}
				break;
			}
		}
		len = strlen(dst);
		if(feof(pf)) {
			fclose(pf);
			pf = NULL;
			pnt++;
		}
	}
	else if(strstr(linee[pnt], "Content-Disposition: attachment;")) {
		len = snprintf(dst, size, "Content-Disposition: attachment; filename=\"%s\"%s",
				rindex(nome, '/')+1, MIMEEND);
		pnt++;
	}
	else if(strstr(linee[pnt], "Content-Type: application/octet-stream")) {
		len = snprintf(dst, size, "Content-Type: application/octet-stream; name=\"%s\"%s",
				rindex(nome, '/')+1, MIMEEND);
		pnt++;
	}
	else {
		len = snprintf(dst, size, "%s%s", linee[pnt++], MIMEEND);
	}
    return len;
}

int mimeEnd(char* dst, int size, int reset) {
static char* linee[] = {
	"--pj+EhsWuS_QJx.x7p_r--",
	"",
	NULL };
static int pnt = 0;
int len = 0;

	if(reset)
		pnt = 0;

	if(linee[pnt]) {
		len = snprintf(dst, size, "%s%s", linee[pnt++], MIMEEND);
	}
	return len;
}

// findFile - finds "n_files" in "folder" with "ext"ension and "delta" seconds near to now
// >0 dimensions of the file found, 0 no files found
int findFile(char* nome, char* folder, char* pattern, char* ext, time_t delta) {
DIR *dir;
struct dirent *ent;
struct stat fs;
char tmpf[300], extf[5];
time_t df;
int size = 0;

    logger(3, "findFile(buf, %s, %s, %d) entering", folder, ext, (int)delta);
    sprintf(extf, ".%s", ext);
    if ((dir = opendir (folder)) != NULL) {

        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                if(!strcasestr(ent->d_name, pattern) || !strcasestr(ent->d_name, extf))
                    continue;

                sprintf(tmpf, "%s/%s", folder, ent->d_name);
                if(stat(tmpf, &fs) != 0) {
                    logger(1, "findFile cannot stat file [%s]", tmpf);
                    continue;
                }
                df = abs(fs.st_mtime - time(NULL));
                if(df < delta) {
                    delta = df;
                    strcpy(nome, tmpf);
                    size = fs.st_size;
                    logger(1, "findFile %s delta time %d", nome, (int)delta);
                }
            }
        }
        closedir (dir);
    } else {
        logger(0, "findFile cannot open dir %d-%s", errno, strerror(errno));
    }
	logger(3, "findfile exiting with [1] file=%s delta=%d", nome, (int)delta);
	return size;
}
 

// setup and array containing the email MIME encoded
int mimeMail(const char* to, const char* from, char* subject, char** testo, CURLBUF *pdata){
register size_t linea, linee;
int len;
char buf[100];
char** nomi;
char* pattern[] = { "tenvis", "webc", 0 };
register int i,j;
#define NFILES (sizeof(pattern)/sizeof(char*))

	logger(3, "mimeMail(%s, %s, %s, testo, pdata) entering", to, from, subject);
    nomi = calloc(sizeof(char*), NFILES+1);
    len = i = 0;
    for(j = 0; pattern[j]; j++) {
        linea = (findFile(buf, "/tmp/mjpg", pattern[j], "jpg", DELTASEC)/2*3)/MIMELEN;
        if(linea) {
            nomi[i] = malloc(strlen(buf)+1);
            strcpy(nomi[i], buf);
            len += linea+1;
            i++;
        }
    }


	//Extra row for our special character to be used in conditional statements,here ""
	linee = 24 + len;
	pdata->data = calloc(linee, sizeof(char*));
	// ADD_SIZE for TO,FROM,SUBJECT,CONTENT-TYPE,CONTENT-TRANSFER-ENCODING,CONETNT-DISPOSITION and \n

	// Header
	for(linea = 0, len = mimeHeader(buf, sizeof(buf)-1, to, from, subject, 1);
		len;
		len = mimeHeader(buf, sizeof(buf)-1, to, from, subject, 0), linea++) {
		pdata->data[linea] = malloc(len+1);
		strcpy(pdata->data[linea], buf);
	}
 
	// testo email
	for(len = mimeTesto(buf, sizeof(buf)-1, testo, 1);
		len;
		len = mimeTesto(buf, sizeof(buf)-1, testo, 0), linea++) {
		pdata->data[linea] = malloc(len+1);
		strcpy(pdata->data[linea], buf);
	}

	// attachemnt
	for(i=0; i<NFILES; i++) {
		if(nomi[i]) {
			for(len = mimeAttach(buf, sizeof(buf)-1, nomi[i], 1);
				len > 0 && linea < linee;
				len = mimeAttach(buf, sizeof(buf)-1, nomi[i], 0), linea++) {
				pdata->data[linea] = malloc(len+1);
				strcpy(pdata->data[linea], buf);
			}
			if(linea == linee) {
				logger(0, "mimeMail Need to redim lines buffer: %d\n", linea);
				logger(3, "mimeMail exiting 0");
				return 0;
			}
		}
	}

	for(len = mimeEnd(buf, sizeof(buf)-1, 1);
		len && linea < linee;
		len = mimeEnd(buf, sizeof(buf)-1, 0), linea++) {
		pdata->data[linea] = malloc(len+1);
		strcpy(pdata->data[linea], buf);
	}
    pdata->data[linea] = NULL;
	
	pdata->linea = 0;

	logger(3, "mimeMail exiting, linee=%d", linea);
	return linea;
}
 
// this function is called during curl_easy_perform() to prepare data to send to website, with 
// size*nmemb bytes; the "memory" of this is managed by userp
static size_t curlCallBack(void *ptr, size_t size, size_t nmemb, void *userp)
{
	CURLBUF *mail = (CURLBUF *)userp;
	int lun = 0;
 
	if((size*nmemb) < 1) 
		return 0;
 
	if(mail->data[mail->linea]) {
		lun = strlen(mail->data[mail->linea]);

		if(lun >= size*nmemb) {
			memcpy(ptr, mail->data[mail->linea], size*nmemb);
			memmove(mail->data[mail->linea],
					mail->data[mail->linea]+size*nmemb, lun - size*nmemb);
			lun = size*nmemb;
		}
		else {
			memcpy(ptr, mail->data[mail->linea], lun+1);
			printf("%d-%s", mail->linea, mail->data[mail->linea]);
			mail->linea++;
		}
	}
	return lun;
}

void discardArray(CURLBUF* lineeBuf) {
register int i;
	for(i = 0; lineeBuf->data[i]; i++) {
		printf("%-2d-%s", i, lineeBuf->data[i]);	
		free(lineeBuf->data[i]);
	}
  	free(lineeBuf->data);
	lineeBuf->linea = 0;
	*(lineeBuf->data) = 0;
}


// sendMail - send email to TO with text specified, it finds the latest files in /tmp/mjpg folder
int sendMail(char* subject)
{
CURL *curl;
CURLcode res = CURLE_OK;
struct curl_slist *recipients = NULL;
CURLBUF dataOrigin;
size_t linee;
char* FROM = "morgia@email.it";
char* TO   = "gmoroni@gmail.com";
char* testo[] = { 
#ifdef OK_MJPG
		"Activated IRDA and MJPG is active at http://glmnas.no-ip.org:51417/?action=stream",
#else
		"IRDA alarm detected, pictures following",
#endif
					0 };

	logger(3, "sendMail(%s)  entering", subject);


	if((linee = mimeMail(TO, FROM, subject, testo, &dataOrigin)) < 0) {
		logger(0, "sendMail mimeMail() failed");
		logger(3, "sendMail exiting 0");
		return 0;
	}


	if(pthread_mutex_lock(&curlMTX)) {
		logger(0, "sendMail lock error: %s", strerror(errno));
		return 0;
	}
	curl = curl_easy_init();
	recipients = NULL;
	if(curl) 
	{
		curl_easy_setopt(curl, CURLOPT_USERNAME, FROM);
		curl_easy_setopt(curl, CURLOPT_PASSWORD, "passwd00");
		curl_easy_setopt(curl, CURLOPT_URL, "smtp://smtp.email.it:587");

		curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); // disable SSL verification
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); // disable SSL verification
		//curl_easy_setopt(curl, CURLOPT_CAINFO, "google.pem");

		curl_easy_setopt(curl, CURLOPT_MAIL_FROM, FROM);
		recipients = curl_slist_append(recipients, TO);
		curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

		curl_easy_setopt(curl, CURLOPT_READFUNCTION, curlCallBack);
		curl_easy_setopt(curl, CURLOPT_READDATA, &dataOrigin);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
		if(logOn > 2 || logOnI > 2) {
			curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); // enable verbose output
			curl_easy_setopt(curl, CURLOPT_STDERR, logF); // and put into logfile
		}

		res = curl_easy_perform(curl);
		if(res != CURLE_OK) {
			logger(0, "sendMail curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}

		curl_slist_free_all(recipients);
		curl_easy_cleanup(curl);
		if(pthread_mutex_unlock(&curlMTX))
			logger(0, "sendMail unlokMTX=%s", strerror(errno));
	}
	discardArray(&dataOrigin);
	logger(3, "sendMail exiting 1");
	return 1;
}


// function to write to file the data obtained from CURL library
size_t httpWrite(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}
size_t httpMem(void *ptr, size_t size, size_t nmemb, string *s)
{
  size_t new_len = s->l + size*nmemb;
  s->s = s->s ? realloc(s->s, new_len+1) : malloc(new_len+1);
  memcpy(s->s+s->l, ptr, size*nmemb);
  s->s[new_len] = '\0';
  s->l = new_len;

  return size*nmemb;
}
// connect to website (with auth string if present) and write to file or to mem 
// return <0 if lock, 0 if error or >0 if ok
int curlOp(const char* url, const char* auth, const bool up, FILE* pf, string* mem) {
struct stat file_info;

	logger(3, "curlOp(%s,%s,%d,...)", url, auth, up);
	if(pthread_mutex_lock(&curlMTX)) {
		logger(0, "curlOp lock error: %s", strerror(errno));
		return -1;
	}
	if((curl = curl_easy_init())) {
		/* First set the URL that is about to receive our POST. This URL can just as well be
			a https:// URL if that is what should receive the data. */
		curl_easy_setopt(curl, CURLOPT_URL, url);
		if(!up) curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10); // set 10 seconds the connection timeout
		if(strcasestr(url, "https")) {
			curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); // disable SSL verification
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); // disable SSL verification
		}
		if(logOn > 3 || logOnI > 3) {
			curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); // enable verbose output
			curl_easy_setopt(curl, CURLOPT_STDERR, logF); // and put into logfile
		}
		if(auth) curl_easy_setopt(curl, CURLOPT_USERPWD, auth);
		if(pf) {
			if(up) {
				if(fstat(fileno(pf), &file_info) == 0) {
					curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
					curl_easy_setopt(curl, CURLOPT_READDATA, pf);
					curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,
													(curl_off_t)file_info.st_size);
				}
				else {
					logger(3, "curlOp fstat(): %s", strerror(errno));
				}
			}
			else {
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, httpWrite);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, pf);
			}
		}
		else if(mem) {
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, httpMem);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, mem);
		}

		/* Perform the request, res will get the return code */
		if((res = curl_easy_perform(curl)) != CURLE_OK)
			logger(2, "curlOp %s failed: %s", url, curl_easy_strerror(res));

		/* always cleanup  and mutex unlock*/
		curl_easy_cleanup(curl);
		if(pthread_mutex_unlock(&curlMTX))
			logger(0, "sendMail unlokMTX=%s", strerror(errno));
	}
	logger(3, "curlOp exiting [%d]", (res == CURLE_OK));
	return (res == CURLE_OK);
}


// POST to upload data to thingSpeak, channel has a key as below
// return 0 if KO and 1 if OK
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

int thingSpeak(	const char	*key,		// thingSpeak key
				TSFLDS_T	*data) {	// vectors of values to publish
#define HOST "api.thingspeak.com"
#define PAGE "/update"
#define PORT 80
#define MSGSZ 80 // maximum size of parameters segmentation
     
const char FMTpost[] =
"POST %s HTTP/1.1\n\
Host: %s\n\
Connection: close\n\
X-THINGSPEAKAPIKEY: %s\n\
Content-Type: application/x-www-form-urlencoded\n\
Content-Length: %d\n\
\n\
%*.*s\n";
static struct sockaddr_in remote;
int sock = 0;
int pass, tmpres, len, sent, KO;
register char *pt;
register short i;
char buf[BUFSIZ+1],tmp[200],fnc[30];
static struct TIME_FILTER {
	char key[40];
	time_t secs;
	TSFLDS_T fld[MAX_TSFLDS];
} tf[XRF_MAX];
struct TIME_FILTER *ptf;

     
	if(pthread_mutex_lock(&thingSpeakCall)) {
		logger(0, "thingSpeak entering [%s] lock=%s exiting", key, strerror(errno));
		return 0;
	}
	logger(3, "thingSpeak entering [%s] lock=OK", key);
	KO = 1;
	buf[0] = 0;
	// filtering of requests based on frequency, store last data for usage with new call
	for(i = 0, ptf = NULL; i < XRF_MAX; i++) {
		if(CK_MSG(tf[i].key, key)) {
			ptf = &(tf[i]);
			break;
		}
		else if(tf[i].key[0] == 0) {
			strcpy(tf[i].key, key);
			ptf = &(tf[i]);
			break;
		}
	}
	if(ptf) {
		for(i = 0; i < MAX_TSFLDS; i++) {
			switch(data[i].st) {
			case 1: ptf->fld[i].v.vi = data[i].v.vi; ptf->fld[i].st = 1;
				logger(3, "thingSpeak fld[%d].v.vi=[%d]", i, ptf->fld[i].v.vi);
				break;
			case 2: ptf->fld[i].v.vf = data[i].v.vf; ptf->fld[i].st = 2;
				logger(3, "thingSpeak fld[%d].v.vf=[%2.3f]", i, ptf->fld[i].v.vf);
				break;
			}
		}
		if((time(NULL) - ptf->secs) > 600) { // enough time, send data to channel
			memset(tmp, 0, sizeof(tmp));
			for(pt = tmp, i = 0; i < MAX_TSFLDS; i++) {
				if(ptf->fld[i].st == 1) {
					pt+=sprintf(pt, "field%d=%d&", i+1, ptf->fld[i].v.vi);
					ptf->fld[i].st = 0;
					ptf->fld[i].v.vi = 0;
				}
				else if(ptf->fld[i].st == 2) {
					pt+=sprintf(pt, "field%d=%2.3f&", i+1, ptf->fld[i].v.vf);
					ptf->fld[i].st = 0;
					ptf->fld[i].v.vf = 0.0;
				}
			}
			len = strlen(tmp)-1;
			sprintf(buf, FMTpost, PAGE, HOST, key, len, len, len+1, tmp);
			ptf->secs = time(NULL);
			logger(3, "thingSpeak POST is:\n<<START>>\n%s<<END>>", buf);
			KO = 0;
		}
		else {// save data but overwrite values for same fields otherwise concatenate
		}
	}
	if(!ptf || KO) {
		if(pthread_mutex_unlock(&thingSpeakCall))
			logger(0, "thingSpeak exiting [%d] %sunlock=%s", ptf!=NULL, ptf?"":"no-key ", strerror(errno));
		else
			logger(3, "thingSpeak exiting [%d] %sunlock=OK", ptf!=NULL, ptf?"":"no-key ");
		return (i!=XRF_MAX);
	}
	errno = 0;

	if(buf[0] != 0) for(pass = 1; pass < 3; pass ++) {
		strcpy(fnc, "socket");
		if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) > 0) {
		char ip[16]; // from URL to IP address
		struct hostent *hent;

			strcpy(fnc, "gethostbyname");
			if((hent = gethostbyname(HOST)) != NULL)
			{
				strcpy(fnc, "inet_ntop");
				if(inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, sizeof(ip)) != NULL) {
					logger(3, "thingSpeak IP is %s", ip);
					remote.sin_family = AF_INET;
					strcpy(fnc, "inet_pton");
					// from ip to data structure
					if(inet_pton(AF_INET, ip, (void *)(&(remote.sin_addr.s_addr))) > 0) {
						remote.sin_port = htons(PORT);
						strcpy(fnc, "connect");
						if(connect(sock, (struct sockaddr *)&remote, sizeof(struct sockaddr))>0)
							logger(3, "thingSpeak socket %d connected", sock);
					}
				}
			}
		}
		if(errno) { // failure in send command above, recconnect
			logger(1, "thingSpeak %s() %d-%s", fnc, errno, strerror(errno));
			close(sock);
		}
		else {
			//Send the query to the server, it can be packetized
			for(sent = 0, len = strlen(buf); sent < len; sent += tmpres) {
				if((tmpres = send(sock, buf+sent, len-sent, 0)) < 0) {
					logger(1, "thingSpeak send error %d-%s, RECONNECT", errno, strerror(errno));
					break;
				}
			}
			if(sent == len) { // POST completed
				memset(buf, 0, sizeof(buf));
				strcpy(fnc, "recv");
				ptf->secs = time(NULL);
				for(pt=buf; (tmpres = recv(sock, pt, BUFSIZ-(pt-buf), 0)) > 0; pt+=tmpres);
			}
			break;
		}
	}
	if(errno) {
		sprintf(buf, "%s(%s) %d-%s ", fnc, HOST, errno, strerror(errno));
		KO = 0;
	}
	else {
		strcpy(buf, "");
		KO = 1;
	}
	if(sock) close(sock);
	if(pthread_mutex_unlock(&thingSpeakCall))
		logger(0, "thingSpeak exiting [%d] %sunlock=%s", KO, buf, strerror(errno));
	else
		logger(3, "thingSpeak exiting [%d] %sunlock=OK", KO, buf);
	return KO;
}


// update the time string user to log message
inline char* upt() {
	time_t curtime;
	struct tm ora;
	static char mesora[15]; // string with month-day-time updated with upt()
	static time_t oldtime = 0;

	curtime = time(NULL);
	if((curtime - oldtime) > 1) {
		oldtime=curtime;
	
		localtime_r(&curtime, &ora);
		sprintf(mesora, "%02d%02d-%02d%02d%02d", ora.tm_mon+1, ora.tm_mday,
				ora.tm_hour, ora.tm_min, ora.tm_sec);
	}
	return mesora;
}


inline char* byte2bin(unsigned char v) {
register short i;
static char r[9];

    for(i = 7; i >= 0; i--) {
        r[i] = '0' + (v & 0x01);
        v >>= 1;
    }
    r[8] = 0;
    return r;
}


// overload the fprintf to handle logging to file correctly
//  logT is the logType level
// the line below is useful for GCC complile time arguments checking
void _logger(int type, int level, char* fmt, ...)
{
va_list args;
time_t curtime;
static time_t oldtime = 0;
static char fmtOra[15];
char fmtl[500];
struct stat stFile;
int ris;

	if(pthread_mutex_lock(&_loggerMTX)) {
		fprintf(logF, "0000-000000 _logger lock MTX: %s", strerror(errno));
		return;
	}
	if(level <= type) {
		curtime = time(NULL);
		if((curtime - oldtime) >= 1) {
		struct tm ora;
			
			oldtime=curtime;
	
			localtime_r(&curtime, &ora);
			sprintf(fmtOra, "%02d%02d-%02d%02d%02d", ora.tm_mon+1, ora.tm_mday,
													 ora.tm_hour, ora.tm_min, ora.tm_sec);
		}
		sprintf(fmtl, "%s %d %s", fmtOra, level, fmt);

		// try to detect if logfile is rotated or not and reopen file
		ris = stat(logFN, &stFile);
		if((ris < 0 && errno == ENOENT) || stFile.st_size < 3) {
			fclose(logF);
			logF = fopen(logFN, "a+");
			fprintf(logF, "%s 1 _logger: detected file rotation\n", fmtOra );
		}


		va_start(args, fmt); // initialize the list of arguments
		vfprintf(logF, fmtl, args);
		if(level < 2) { // completely flush the log every call
		    fflush(logF);
   			fsync(fileno(logF));
		}
		va_end(args);
	}
	pthread_mutex_unlock(&_loggerMTX);
}
