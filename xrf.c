// xrf.c module with XRF fucntions, to be linked with gpmon.c
// gcc -o gpmon  gpmon.c xrf.c -l wiringPi -l m -l pthread
// ThingSPeak API Write KEY 8GL51FPSEAAHQ2GL 

#include "gpmon.h"
#include <signal.h>
#include <termios.h>
#include <sys/time.h>
#include <ctype.h>

// global definitions and variables
#define XRF_MAX 10
#define BATTP 5 // battery reading after BATTP awakenings
#define LUCEP 3 // light level reading after LUCEP awakenings
#define THSLEEPTIME "180S" // sleeping time for TH XRF
#define CTSLEEPTIME "020S" // sleeping time for CT XRF




int sd;			// serial descriptor to dialog with XRF modules

// prototypes
void thermostat (char* buf);
int heaterOn(float temp);
void XRFthingSpeak(	struct ST * xrf);
void CTXRFalarm(int sig);			// signal hander for timed XRF light sensor reading
void powerBox (char* buf);			// function to manage CT XRF

// prints in a buffer the status of one or all of the XRF (if xrf is NULL)
// return a pointer to the passed buffer
char* printXRFStatus(struct ST *xrf)
{
static char buf[600];
register char* pt;
register short j, ind;
register time_t val;
struct ST *x;
struct tm ora;

	memset(buf, 0, sizeof(buf));

	if(xrf == NULL) {
		j = 0;
		x = &st[j];
	}
	else {
		j = -1;
		x = xrf;
	}

	pt = buf;
	do {
		pt += sprintf(pt, "%2.2s %4.2fV light=%4.3f temp=%05.2f", x->name, x->batt, x->light, x->temp);
		if(x->name[0] == 'T' && x->name[1] == 'H') 
			pt += sprintf(pt, "(%'5.0fR) relay=%d-%d", x->ntc, x->relayOn, x->relayOnMan);

		if(x->name[0] == 'C' && x->name[1] == 'T') {
			pt += sprintf(pt, " IR%-5d Humid=%4.3f PIN=%*.*s", x->irOn, x->umidita, MAX_PIN, MAX_PIN, x->o);
		}
			
	//	localtime_r(&x->timeLastAnsw, &ora);
	//	pt += sprintf(pt, "[%12.12s%s%.12s]@%02d:%02d:%02d\n", 
	//				x->msg, x->manualMsg[0]?",":"", x->manualMsg,
	//				ora.tm_hour, ora.tm_min, ora.tm_sec);
		if(x->timeLastAnsw == 0)
			val = -1;
		else
			val = time(NULL) - x->timeLastAnsw;
		pt+=sprintf(pt, " [%12.12s%s%.12s]%dsec\n", x->msg, x->manualMsg[0]?",":"", x->manualMsg, val);
		j++;
		x = &st[j];
	} 
	while(j > 0 && x->name[0]);
	*(pt-1) = 0;
	return buf;
}

void writeCmd(FILE* pf, char* str) {
struct timeval tv;
struct tm ora;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &ora);

	fprintf(pf, str[0] == 'a' ?  "%02d%02d-%02d:%02d:%02d.%-6d a  XRF [%12.12s]\n" :
								 "%02d%02d-%02d:%02d:%02d.%-6d da XRF [a%11.11s]\n",
		ora.tm_mon+1, ora.tm_mday, ora.tm_hour, ora.tm_min, ora.tm_sec, (int)tv.tv_usec, str);
	fflush(pf);
}

static inline char* fmtMsg(char* dst, char* src, int srcLen) {
register int len = 12 - 1 - 2 - srcLen;

	sprintf(dst, "a%s%*.*s", src, len, len, "---------");
	dst[12] = 0;
	return dst;
}

// standard function to format LLAP command, send it to the channel and log it if logging is enabled
void serialSend(const char *dev,	// device to send message
				const char *msg) {	// message to send, if NULL resend last message
	register int len, i;
	struct ST *xrf = NULL;

	loggerX(4, "serialSend entering [%s,%s]", dev, msg);

	len = 12 - 3 - strlen(msg == NULL ? "" : msg);

	// search in memory a previous device/message
	for(i = 0; i<XRF_MAX; i++) {
		if(st[i].name[0] == 0) { // first empty position, no device found, allocate buffer for message sent
			xrf = &st[i];
			strncpy(xrf->name, dev, 2); xrf->name[2] = 0;
			memset(xrf->msg, 0, sizeof(xrf->msg));
			memset(xrf->manualMsg, 0, sizeof(xrf->manualMsg));
			if(strncmp(xrf->name, "TH", 2) == 0) {
				strcpy(xrf->sleepTotStr, THSLEEPTIME);
				xrf->sleepTot = atoi(THSLEEPTIME);
			}
			else if(strncmp(xrf->name, "CT", 2) == 0) {
				strcpy(xrf->sleepTotStr, CTSLEEPTIME);
				xrf->sleepTot = atoi(CTSLEEPTIME);
			}
			//xrf->timeLastAnsw = time(NULL);
			xrf->timeLastAnsw = 0;
			xrf->cmd = NULL;
				
			loggerX(1, "Create new device %2.2s message [%s]", xrf->name, msg!=NULL?msg:"<null>");
			break;
		}
		else if(strncmp(dev, st[i].name, 2) == 0) { // found the device position, reuse memory
			xrf = &st[i];
			break;
		}
	}
	if(!xrf) {
		loggerX(0, "serialSend ERROR: no more space in array for %2.2s msg[%s]", dev, msg);
		return;
	}

	// if msg != NULL resend message in memory otherwise use msg previously sent
	if(msg != NULL) {
		sprintf(xrf->msg, "a%2.2s%s%*.*s", dev, msg, len, len, "---------");
	}

	if(xrf->msg[0] == 'a') {
		serialPuts(sd, xrf->msg);
		xrf->timeLastSent = time(NULL);
		if(xrf->cmd) writeCmd(xrf->cmd, xrf->msg);
		loggerX(4, "Sent message [%s]", xrf->msg);
		// if not ACK or HELLO or REBOOT the retry must be set
		if(strstr(xrf->msg, "ACK") == 0 && strstr(xrf->msg, "HELLO") == 0
		&& strstr(xrf->msg, "REBOOT") == 0)
			xrf->msgGone = TRUE;
		else
			xrf->msgGone = FALSE;
	}
	else
		loggerX(0, "Wrong message [%12.12s]", xrf->msg);


	loggerX(4, "serialSend exiting");
	return;
}


// main cycle to handle all of the XRF in the network
void *xrfmon(void *arg)
{
signed char c;
time_t res;
int ind;
char buf[40];
struct termios options;

	loggerX(1, "xrfmon entering");
	
	/* open the serial port Raspberry XRF to be non-blocking (read will return immediatly) */
	sd = serialOpen(MODEMDEVICE, 9600);
	if (sd < 0) {
		perror(MODEMDEVICE);
		exit(-1);
	}
	tcgetattr (sd, &options) ;
	options.c_cc [VTIME] = 20 ;    // Adjust timeout to 2secs (20 deciseconds, override wiringPi)
	tcsetattr (sd, TCSANOW | TCSAFLUSH, &options) ;


	memset(&st,0, sizeof(st));
	//(void)serialGetchar(sd); // this is needed to correctly start the channel

	// at the beginning all of the devices must be queried to load structures
	serialSend("TH", "HELLO");
	serialSend("CT", "HELLO");

	ind = 0;
	memset(buf,0,sizeof(buf));

	/* loop while waiting for input. normally we would do something useful here */ 
	while (TRUE) {
		register struct ST *xrf;
		register int i;

		// read LLAP command that will always start with 'a', in case of new cmd and not 'a' then
		// it continue to drain characters from the buffer until 'a' is found, this is to sync 
		// command this point must be unique because it calls the right routine based on 
		// messages; the XRF management is based on XRF identifier in the message
		if((c=serialGetchar(sd))<0) {
			memset(buf,0,sizeof(buf));
			ind = 0;

			loggerX(4, "xrfMon Cycling through XRFs");
			// every time-out of serialGetchar if there is an unanswered message resend it
			for(i = 0; i<XRF_MAX; i++) {

				// this exit if there are no more XRF to analyse
				if(st[i].name[0] == 0)
					break;
				else
					xrf = &st[i];

				if(xrf->timeLastAnsw != 0 && (time(NULL)%200 < 2))
					loggerX(0, "%s", printXRFStatus(xrf));
				
				// if it has never communicate or delay in answer sends HELLO to see if it is alive
				// the 0.04 in line below is 4% and is a long time for battery saving
				res = time(NULL) - xrf->timeLastAnsw - (int)(0.04*(float)(xrf->sleepTot)+1);
				res -= strncasecmp(st[i].name, "CT", 2) == 0 ? 3 : 0;
				if(xrf->timeLastAnsw == 0) { // never answered
					if((time(NULL)%600) == 0) // every some minutes send HELLO
						serialSend(xrf->name, "HELLO");
				}
				else if(res > xrf->sleepTot) {
					if(res < 2*xrf->sleepTot) { // reasonable timeout
						loggerX(2, "xrfMon Delay of %dsec from XRF %2.2s > %dsec",
								res, xrf->name, (int)xrf->sleepTot);
						serialSend(xrf->name, "HELLO");
						xrf->fails = 0; // put counter a zero to avoid unnecessary REBOOT
					}
					else if(res%3600 == 0) { // maybe XRF is blocked try anyway a reboot
						loggerX(1, "xrfMon Delay of %dsec from XRF %2.2s > %dsec, try reboot",
								res, xrf->name, (int)xrf->sleepTot);
						serialSend(xrf->name, "REBOOT");
						xrf->on = FALSE;
					}
				}
				else if(xrf->msgGone) {
                    // tooo much resend then REBOOT otherwise tries to resend message
					if(xrf->fails > 100) {
						loggerX(0, "xrfMon request a REBOOT %s for too many timeout", xrf->name);
						fflush(logF);
						serialSend(xrf->name, "REBOOT");
						xrf->on = FALSE;
						xrf->fails = 0;
					}
					// resend once the command stored after 3 sec without answer
					else if(xrf->timeLastSent > (xrf->timeLastAnsw+xrf->sleepTot+3)) {
						serialSend(xrf->name, NULL);
						xrf->fails++;
						//xrf->msgGone = FALSE;
					}
				}
			}
		}
		else if (c == 'a') {
			if (buf[0] != 0) { // there are characters lost in previous message, print and discard them
				loggerX(0, "xrfMon Previous incomplete answer [%12.12s] discard it", buf);
			}
			memset(buf,0,sizeof(buf));
			ind = 0;
			continue;
		}
		else {
			buf[ind++] = c;
			while (ind < 11 && serialDataAvail(sd) > 0 )
			{
				buf[ind++] = serialGetchar(sd);
			}
		}
		
		if(ind < 11) continue; // fragmented message, something more to read
		ind = 0;
		
		loggerX(4, "xrfMon LLAP message [a%s]", buf);
		fflush(logF);
		fsync(fileno(logF));
		
		for(i = 0; i<XRF_MAX; i++) {
			if(strncasecmp(st[i].name, buf, 2) == 0)
			{
				xrf = &st[i];
				//xrf->timeLastAnsw = time(NULL);
				//xrf->msgGone = FALSE;
				if(xrf->cmd) writeCmd(xrf->cmd, buf);
				break;
			}
		}
		
		if(CK_MSG(&buf[2], "STARTED")) {
			loggerX(0, "xrfMon Device ->%2.2s<- ALIVE", buf);
			xrf->on = TRUE;
		}
		else if(CK_MSG(&buf[2], "REBOOT")) {
			loggerX(0, "xrfMon Device %2.2s REBOOTING", buf);
			xrf->on = TRUE;
		}
		// events to be logged in any case, if logging enabled and send mail if necessary
		else if(CK_MSG(&buf[2], "BATTLOW")) {
		char comando[130];
		
			res = time(NULL) - xrf->mailTime;
			if(res > 12*3600) {
				sprintf(comando, "echo 'XRF %s check it'|mail -s 'XRF %2.2s low battery at %s' gmoroni@gmail.com", xrf->name, xrf->name, upt());
				system(comando);
				loggerX(0, "xrfMon Device %2.2s ALARM==>[a%s]", xrf->name, buf);
			}
		}
		else if(CK_MSG(&buf[2], "ERROR")) {
			loggerX(0, "xrfMon Device %2.2s ERROR==>[a%s]", xrf->name, buf);
		}

		// management of the devices
		if(CK_MSG(buf, "TH")) { 
			thermostat(buf);
		}
		else if(CK_MSG(buf, "CT")) {
			powerBox(buf);
		}
		else
			loggerX(0, "xrfMon Unknown transmitter %2.2s [a%s]", buf, buf);
		fflush(logF);
		fsync(fileno(logF));

		// resetting data for next message
		ind = 0;
		memset(buf,0,sizeof(buf));
	}
	serialClose(sd);
}

// thermostat state machine, the status passages are
// [STARTED]->HELLO or AWAKEN[->BATT][->OUTC1->ANAA->OUTC0]->OUTD1->ANAA->OUTD0->OUTA/OUTB->SLEEP
// this means that from eventual [STARTED] status (the device can be already on, only program restart)
// the alive time is less than 1/10sec with sensors and communication, rest of time is sleeping
// the XRF has these commands:
// HELLO: to ping the device if it is alive
// BATT: read battery every some cycles
// OUTC1/0: turn on/off light sensor to read light values
// OUTD1/0: turn on/off to read NTC sensor for temperature
// ANAA: read ADC light sensors according to OUTC(sensor with 10k voltage divider)
// ANAB: read ADC NTC sensor according to OUTD (sensor with 10k voltage divider)
// ANAD: read ADC to see relay status (not yet implemented)
// OUTA/OUTB: turn on/off the bistable relay to drive heater
// SLEEP: put in sleep mode and start from the beginning
// there is some problems with loosing messages, it has been fixed with main cycle, it resends requests

void thermostat (char* buf) // command received
{
static struct ST *xrf = NULL; // pointer to ST struct of XRF devices
static int battC = 0; // counter for awakenings to ask for battery value
static int luceC = 0; // counter for awakenings to ask for environmental light value
char cmd[20]; // command to send to TH before exit

	loggerX(3, "thermostat entering [%s]", buf);


	memset(cmd, 0, sizeof(cmd));
	// this configure the pointer to avoid further searches
	if(xrf == NULL) {
		register int i;
	
		for(i=0; i<XRF_MAX; i++) {
			if(strncmp(st[i].name, buf, 2) == 0) {
				xrf = &st[i];
				xrf->relayOn = 0;
				xrf->relayOnMan = 0;
			}
		}
	}

	xrf->fails = 0; // message received, reset flag for waiting message
	xrf->msgGone = FALSE;
	xrf->on = TRUE;
	buf+=2; // skip XRF name to quick analyse message

	if(CK_MSG(buf, "SLEEPING")) {
	FILE* thdb;
	short relay = (xrf->relayOn+xrf->relayOnMan)? 1:0;

		XRFthingSpeak(xrf);

		// write values to db
		if((thdb = fopen(THFILE, "w")) == NULL) {
			loggerX(0, "[TH] file=%s errno=%d-%s", THFILE, errno, strerror(errno));
		}
		else {
			fprintf(thdb, "%d %4.2f %4.2f %4.3f\n", relay, xrf->temp, xrf->batt, xrf->light);
			fclose(thdb);
		}

		battC++; // counter for waiting NTC request
		luceC++; // counter for waiting light request
		loggerX(3, "thermostat exiting for sleeping");
		return;
	}
	// restart/first turning ON of the device
	else if(CK_MSG(buf, "STARTED")) {
		serialSend("TH", "ACK"); // message without confirmation

		strcpy(cmd, "BATT");// HELLO command to start the status machine
	}
	// device awake (sometimes starts sending hello instead of awake)
	else if(CK_MSG(buf, "AWAKE") || CK_MSG(buf, "HELLO")) {
		// Check if it is time to ask for battery level or for light sensor otherwise temperature
		if(battC > BATTP || xrf->batt == 0.0) {
			battC = 0;
			strcpy(cmd, "BATT");
		}
		else if(luceC > LUCEP || xrf->light == 0) {
			strcpy(cmd, "OUTC1");
		}
		else
			strcpy(cmd, "OUTD1");
	}
	// manage battery status
	else if(CK_MSG(buf, "BATT")) {
	float bt;

		sscanf(buf, "BATT%f-", &bt);
		xrf->batt = bt;
		battC = 0;

		if(luceC > LUCEP || xrf->light == 0)
			strcpy(cmd, "OUTC1");
		else
			strcpy(cmd, "OUTD1");
	}
	// manage light sensor ON with passage to ANAC status or OFF
	else if(CK_MSG(buf, "OUTC")) {
	//const struct timespec pausa = {0, 50000000}; // 50 msec to stabilize light sensor

		// received confirmation of light sensor ON, then send ANAD command otherwise 
		// received confirmation of light sensor OFF continue with temperature
		if(buf[4] == '1') {
			//nanosleep(&pausa, NULL); // wait for light sensor stabilization
			strcpy(cmd, "ANAA");
		}
		// resume standard status operation
		else {
			strcpy(cmd, "OUTD1");
		}
	}
	// enable thermistor and eventually the thermostat relay
	else if(CK_MSG(buf, "OUTD")) {
	char tmp[20] = "";
	//const struct timespec pausa = {0, 50000000}; // 50 msec to stabilize temp

		if(buf[4] == '1') { // the thermistore is on
			//nanosleep(&pausa, NULL); // wait for temp stabilization
			strcpy(tmp, "ANAB"); // read thermistor ADC
		}
		else { // the thermistor is off, time to manage the relay
			// activation of thermostat relay, there is a security limit of 23C to disconnect relay
			// turn ON relay due to heaterON function or manually requested 
			if(xrf->temp > 23 && xrf->relayOn) { // safety limit
				strcpy(tmp, "OUTB1");
				loggerX(1, "thermostat temp above protection limit OFF relay");
			}
			else if(heaterOn(xrf->temp) || xrf->relayOnMan) {
				if(!(xrf->relayOn)) strcpy(tmp, "OUTA1"); // set relay
			}
			else {
				if(xrf->relayOn) strcpy(tmp, "OUTB1"); // reset relay
			}
		}

		// if a command is necessary to set bistable relay it is sent, otherwise put sleeping 
		// (if no manualMsg message is available)
		if(tmp[0])
			strcpy(cmd, tmp);
		else if(xrf->manualMsg[0] != 0) {
			strcpy(cmd, xrf->manualMsg);
			memset(xrf->manualMsg, 0, sizeof(xrf->manualMsg));
		}
		else{
			sprintf(cmd, "SLEEP%s", xrf->sleepTotStr);
		}
	}
	// read ADC value of the light sensor
	else if(CK_MSG(buf, "ANAA")) {
		xrf->light = atoi(&buf[4])/2047;
		luceC = 0;
		strcpy(cmd, "OUTC0");
	}
	// read ADC value of thermistor voltage
	else if(CK_MSG(buf, "ANAB")) {
	register float r2;
	int val = atoi(&buf[4]);

		// here the conversion from ANAA to temp in celsius with ADC values and then Steinhart-Hart
		// formula [1/{A+B(LnR)+C(LnR)^3}] - (273.15) with
		// A=8.8570897E-04, B=2.5163902E-04, C=1.9289731E-07
		// Rvar = R*(Vcc-V)/V where V,Vcc(it is 2047, maximum) are the voltage ADC converted values
		xrf->ntc = ((2047.0/(float)val-1.0)*10000.0);
		r2 = logf(xrf->ntc);
		xrf->temp = 1/(8.8570897E-04+2.5163902E-04*r2+1.9289731E-07*pow(r2,3))-273.15;
		xrf->temp = xrf->temp + pow(xrf->batt,2)/(0.0015*xrf->ntc);
		strcpy(cmd, "OUTD0");
	}
	// check confirmation of the enabling/resetting of the relay
	else if(CK_MSG(buf, "OUTA") || CK_MSG(buf, "OUTB")) {
	const struct timespec pausa = {0, 20000000}; // 40msec to enable relay

		xrf->relayOn = buf[3] == 'A' ? TRUE : FALSE; // OUTA1->relay ON, OUTB1->relay OFF
		if(buf[4] == '1') {
			nanosleep(&pausa, NULL); // wait to relay accept command
			sprintf(cmd, "%4.4s0", buf); // end of the management of relay
		}
		else {
			if(xrf->manualMsg[0] == 0)
				sprintf(cmd, "SLEEP%s", xrf->sleepTotStr);
			else {
				strcpy(cmd, xrf->manualMsg);
				memset(xrf->manualMsg, 0, sizeof(xrf->manualMsg));
			}
		}
	}
	else if(CK_MSG(buf, "REBOOT")) {
		strcpy(cmd, "HELLO");
	}
	// Any other message
	else {
		loggerX(0, "thermostat received unhandled message [%s]", buf);
		sprintf(cmd, "SLEEP%s", xrf->sleepTotStr);
	}
	serialSend("TH", cmd);
	xrf->timeLastAnsw = time(NULL);
	loggerX(3, "thermostat exiting");
}

int heaterOn(float temp) {
struct tm ora;
time_t adesso = 0;
int ris = 0, ind;


	if(cfg.spento == -1) { // Array not loaded
	}

	if(cfg.spento == 0) {
		adesso = time(NULL);
		localtime_r(&adesso, &ora);
		adesso = ora.tm_hour*100+ora.tm_min;

		for(ind = 0, ris = 0; ind < MAX_RANGES; ind++) {
			if(adesso > cfg.t[ora.tm_wday][ind].start && adesso < cfg.t[ora.tm_wday][ind].end
					&& temp < cfg.t[ora.tm_wday][ind].temp)
				ris = 1;
		}
	}
	loggerX(3, "heaterOn ris=%d", ris);
	return ris;
}



// prepare data for thingSpeak and if necessary publish it
void XRFthingSpeak(struct ST *xrf)
{
register char* key;
TSFLDS_T data[MAX_TSFLDS];
	

	loggerX(3, "XRFthingSpeak entering XRF=%2.2s", xrf->name);
	memset(&(data[0]), 0, sizeof(data));
	data[0].st = 2; data[0].v.vf = xrf->temp;
	data[1].st = 2; data[1].v.vf = xrf->light;
	if(CK_MSG(xrf->name, "TH")) {
		data[2].st = 1; data[2].v.vi = xrf->relayOn;
		data[3].st = 2; data[3].v.vf = xrf->batt;
		key = THINGS_CH1_KEY;
	}
	else if(CK_MSG(xrf->name, "CT")) {
		data[2].st = 2; data[2].v.vf = xrf->umidita;
		data[3].st = 1; data[3].v.vi = xrf->irOn;
		data[4].st = 2; data[4].v.vf = xrf->batt;
		key = THINGS_CH2_KEY;
	}
	else {
		loggerX(1, "XRFthingSpeak exiting with no data sent");
		return;
	}

	thingSpeak(key, data);
	loggerX(3, "XRFthingSpeak exiting");
}



// used to manage commands for XRF radio modules 
char* XRFcmd (char* buf)	// command received
{
static char message[200];
register int i,j,car;
char *pt;
char *dst;
char tmp[15];
short aBatteria = 0;
struct ST *xrf;

	sprintf(message, "Command [%s] for XRF %2.2s not in list", buf, buf);
	memset(tmp, 0, sizeof(tmp));
	buf[0] = toupper(buf[0]);
	buf[1] = toupper(buf[1]);

	for(j = 0; j<XRF_MAX; j++) {
		if(strncmp(st[j].name, buf, 2) == 0) { // found the device position
			pt = buf + 2; while(*pt == ' ') pt++; // skip optional blank between XRF name and CMD
			xrf = &st[j];
			break;
		}
	}
	if(j == XRF_MAX) {
		return message;
	}
	else {
		sprintf(message, "Command [%s] for XRF %2.2s not valid/understood", buf, buf);
			
		if(strncasecmp(pt, "REL", 3) == 0) {
			if(strncmp(xrf->name, "TH", 2)) {
				xrf->relayOnMan = strcasestr(pt, "ON", 2)? 1 : 0;
				sprintf(message, "Command [%s] set relay %d accepted", buf, xrf->relayOnMan);
			}
		}
		else if(strcasestr(pt, "HELLO") || strcasestr(pt, "REBOOT") || strcasestr(pt, "BATT")) {
			for(j=0; j<12; pt[j] = toupper(pt[j]), j++);
			serialSend(xrf->name, pt);
			sprintf(message, "Command [%s] sent to %2.2s as %s", buf, xrf->name, pt);
		}
		else if(strstr(pt, "CHDEVID")) {
			if(strlen(pt) == (7+2)) {
				for(j = 0; pt[j]; j++)
					xrf->manualMsg[j] = toupper(pt[j]);
				xrf->manualMsg[j] = 0;
				sprintf(message, "Command [%s] accepted", xrf->manualMsg);
			}
		}
		else if(strcasestr(pt, "WR")) {
			pt += strlen("WR"); while(*pt == ' ') pt++; // skip blank between CMD and filename

			if(xrf->cmd != NULL) {
				fclose(xrf->cmd);
				xrf->cmd = NULL;
				strcpy(message, "Command logging file closed");
			}
			if((xrf->cmd = fopen(pt, "w")) == NULL)
				sprintf(message, "Failed to open file %s", pt);
			else
				sprintf(message, "Command logging file %s open", pt);
		}
		else if(strcasestr(pt, "INP")) {
			if(strstr(xrf->name, "CT")) {
				for(j = 0; pt[j]; j++)
					xrf->manualMsg[j] = toupper(pt[j]);
				xrf->manualMsg[j++] = 'A';
				xrf->manualMsg[j] = 0;

				sprintf(message, "Command [%s] accepted and queued as %s", buf, xrf->manualMsg);
			}
		}
		else if(strcasestr(pt, "RESET")) {
			pt += strlen("RESET"); while(*pt == ' ') pt++; // skip blank between CMD and filename

			if(strstr(xrf->name, "CT")) {
				if(xrf->o[0] == 0)
					sprintf(message, "Command [%s] cannot be accepted, status not yet loaded", buf);
				else {
					xrf->o[MAX_PIN-1] = 'R';
					sprintf(message, "Command [%s] made a reset request (PIN=%*.*s)",
						buf, MAX_PIN, MAX_PIN, xrf->o);
				}
			}
		}
		else if(strcasestr(pt, "PIN")) {
			pt += strlen("PIN"); while(*pt == ' ') pt++; // skip blank between CMD and filename

			if(strstr(xrf->name, "CT")) {
				sprintf(message, "Command [%s] not valid, discarded!!!", buf);
				if(xrf->o[0] == 0)
					sprintf(message, "Command [%s] cannot be accepted, status not yet loaded", buf);
				else {
					j = toupper(*pt) - 'A';
					if(j >= 0 && j < MAX_PIN) {
						car = toupper(*(pt+1));
						if((j == (MAX_PIN-1) && (car == 'A' || car == 'C'))
						|| (j < (MAX_PIN-1) && (car == '0' || car == '1'))) {

							xrf->o[j] = car;
							sprintf(message, "Command [%s] made PIN %d set to %c (PIN=%*.*s)",
									buf, j+1, xrf->o[j], MAX_PIN, MAX_PIN, xrf->o);
							if(j == (MAX_PIN-1)) {
							int fd;
	
								if((fd = creat(WATERFLAG, S_IRWXU|S_IRWXG|S_IRWXO)) > 0) {
									close(fd);
									strcat(message, "\nCreato file flag ");
									strcat(message, WATERFLAG);
								}
							}
						}
					}
				}
			}
		}
	}
	return message;
}


// if cmd is NULL find first pin changed and detect command to write data and return first one
// if cmd is NOT NULL mark as confirmed the actual and find the next cmd and return it
// o array: PIN1,PIN2,PIN3,PIN4,PIN5,VALVE
// 001->PIN1, 010->PIN2, 011->PIN3, 100->PIN4, 101,110->VALVE, 111->PIN6
// return next cmd to send or NULL if nothing to send or error
char* getCmd(char* cmd, char* oOld, char* oNew) {
register short ind;
#define MAX_CMDS 10
static struct cmds_t {
	char c[7];
	char st; // P=ready to send, C=confirmed
} cmds[MAX_CMDS];
static short cmdInd = 0;
static short pin;


	loggerX(3, "getCmd cmd=%s oOld=%*.*s oNew=%*.*s", cmd?cmd:"<null>", MAX_PIN, MAX_PIN, oOld,
				MAX_PIN, MAX_PIN, oNew);
	if(cmd == NULL) {
		memset(&cmds[0], 0, sizeof(cmds));
		//for(ind = 0; ind < MAX_CMDS; ind++) {
			//cmds[ind].st = 0;
			//memset(cmds[ind].c, 0, sizeof(cmds[ind].c));
		//}
		cmdInd = 0;
		pin = -1;

		// handling toggle of standard pins from 1 to 4 and 7
		for(ind = 0; ind < MAX_PIN-1; ind++) {
			if(oOld[ind] != oNew[ind]) {
				pin = ind<(MAX_PIN-2)?ind + 1:7; // gestione del pin nuovo
				sprintf(cmds[2].c, "OUTC%d", (int)(pin/4));
				sprintf(cmds[1].c, "OUTB%d", (pin-((int)(pin/4))*4)/2);
				sprintf(cmds[0].c, "OUTA%d", pin%2);
				strcpy(cmds[3].c, "OUTD1");
				strcpy(cmds[4].c, "OUTD0");
				cmds[0].st = cmds[1].st = cmds[2].st = cmds[3].st = cmds[4].st = 'P';
				pin = ind;
			}
		}

		// changes in last pin always require two sequence of cmds
		if(pin < 0 && oNew[MAX_PIN-1] != oOld[MAX_PIN-1]) {
			loggerX(3, "getCmd oNew[MAX_PIN-1]='%c'", oNew[MAX_PIN-1]);
			if(oNew[MAX_PIN-1] == 'A') {
				strcpy(cmds[0].c, "OUTA0");
				strcpy(cmds[1].c, "OUTB1");
				strcpy(cmds[2].c, "OUTC1");
			}
			else if(oNew[MAX_PIN-1] == 'R') {
				strcpy(cmds[0].c, "OUTA0");
				strcpy(cmds[1].c, "OUTB0");
				strcpy(cmds[2].c, "OUTC0");
			}
			else {
				strcpy(cmds[0].c, "OUTA1");
				strcpy(cmds[1].c, "OUTB0");
				strcpy(cmds[2].c, "OUTC1");
				strcpy(cmds[5].c, "OUTA1");
				strcpy(cmds[6].c, "OUTB0");
				strcpy(cmds[7].c, "OUTC1");
				strcpy(cmds[8].c, "OUTD1");
				strcpy(cmds[9].c, "OUTD0");
				cmds[5].st = cmds[6].st = cmds[7].st = cmds[8].st = cmds[9].st = 'P';
			}
			strcpy(cmds[3].c, "OUTD1");
			strcpy(cmds[4].c, "OUTD0");
			cmds[0].st = cmds[1].st = cmds[2].st = cmds[3].st = cmds[4].st = 'P';
			pin = MAX_PIN-1;
		}

		if(pin >= 0) {
			loggerX(3, "getCmd PIN=%d %*.*s=>%*.*s cmds={%c-%s;%c-%s;%c-%s;%c-%s;%c-%s} cmdInd=%d",
					pin, MAX_PIN, MAX_PIN, oOld, MAX_PIN, MAX_PIN, oNew,
					cmds[0].st, cmds[0].c, cmds[1].st, cmds[1].c, cmds[2].st, cmds[2].c,
					cmds[3].st, cmds[3].c, cmds[4].st, cmds[4].c, cmdInd);
			return cmds[cmdInd].c;
		}
		else {
			pin = 0;
			loggerX(3, "getCmd for PIN=%d %*.*s=%*.*s, no changes", pin, 
					MAX_PIN, MAX_PIN, oOld, MAX_PIN, MAX_PIN, oNew);
			return NULL; // nothing to do
		}
	}
	else {
		if(cmds[cmdInd].st == 'P' && strstr(cmd, cmds[cmdInd].c)) {
			cmds[cmdInd].st = 'C';
			cmdInd++;
			if(cmdInd < MAX_CMDS)
				loggerX(3, "getCmd for PIN=%d cmds[%d]=%c,%s cmd=%s", pin, cmdInd,
							cmds[cmdInd].st,cmds[cmdInd].c, cmd); 
			if(strstr(cmd, "OUTD0")) { // end of a command sequence
				if(cmdInd >= (MAX_CMDS-1) || cmds[cmdInd+1].st == 0) { // no more commands
					if(oNew[pin] == 'R') {
						oNew[MAX_PIN-2] = oOld[MAX_PIN-2] = '0';
						oNew[MAX_PIN-1] = oOld[MAX_PIN-1] = 'C';
					}
					else
						oOld[pin] = oNew[pin];
				}
				loggerX(3, "getCmd PIN=%d oOld=%*.*s oNew=%*.*s aligned", pin,
									MAX_PIN, MAX_PIN, oOld, MAX_PIN, MAX_PIN, oNew);
			}
			else
				loggerX(3, "getCmd for PIN=%d ind=%d cmds[ind]=%5.5s,%c cmd=%5.5s", pin, cmdInd,
					cmds[cmdInd].c, cmds[cmdInd].st, cmd);
		}


		return cmds[cmdInd].st ? cmds[cmdInd].c : NULL;
	}
}

// Alarm to manage requests to XRF CT also for automated tasks like watering
void  CTXRFalarm(int sig)
{
static struct ST *xrf = NULL; // pointer to ST struct of XRF devices

	loggerX(2, "CTXRFalarm signal");
	signal(SIGALRM, SIG_IGN);        // disable signal

	if(xrf == NULL) {
	register short ind;

		for(ind = 0; ind<XRF_MAX; ind++) {
			if(strncmp(st[ind].name, "CT", 2) == 0) { // found the device position
				xrf = &st[ind];
				break;
			}
		}
	}
	if(cfg.irriga) {
	time_t curtime = time(NULL), ora;
	static time_t oldtime;
	struct tm now;
	char oldValveStatus = xrf->o[MAX_PIN-1];
	struct stat sf;
	bool bypass = FALSE;

		memset(&sf, 0, sizeof(sf));
		if(stat(WATERFLAG, &sf) == 0) {
			if((curtime - sf.st_ctime) < 15*60)
				bypass = TRUE;
			else 
				unlink(WATERFLAG);
		}
		if((curtime - oldtime) > 60) {
			oldtime=curtime;
			localtime_r(&curtime, &now);
			ora = now.tm_hour*3600+now.tm_min*60+now.tm_sec;

			if(!bypass && ora >= cfg.i[now.tm_wday][0].start) {	// 1st watering period
				if(xrf->umidita < cfg.i[now.tm_wday][0].moisture) {
					xrf->o[MAX_PIN-1] = 'A';
					if(ora > cfg.i[now.tm_wday][0].stop)
						xrf->o[MAX_PIN-1] = 'C';
				}
				else
					loggerX(3, "CTXRFalarm moisture level avoid to start watering");
			}
			if(!bypass && cfg.i[now.tm_wday][1].start			// 2nd watering period (optional)
			&& ora >= cfg.i[now.tm_wday][1].start) {
				if(xrf->umidita < cfg.i[now.tm_wday][1].moisture) {
					xrf->o[MAX_PIN-1] = 'A';
					if(ora > cfg.i[now.tm_wday][1].stop)
						xrf->o[MAX_PIN-1] = 'C';
				}
				else
					loggerX(3, "CTXRFalarm moisture level avoid to start watering");
			}
		}
		if(xrf->o[MAX_PIN-1] != oldValveStatus)
			loggerX(1, "CTXRFalarm turning %s watering", xrf->o[MAX_PIN-1]=='A'?"ON":"OFF");
	}

	serialSend("CT", "ANAA");
	signal(SIGALRM, CTXRFalarm);     // reinstall the handler
	loggerX(2, "CTXRFalarm signal exiting");
}


// Handle messages for power box to drive signals, very often checked the INP status
// status machine: STARTED->[RECOVER_STATUS-INPABCD->][BATT->]ANAA->ANAB->ANAC->ANAD->
// [OUT[ABC]{3}->OUTD]
// the last two commands can be repeated, then final status is written
// REALLY CAREFUL: pin 5 is valve, its values are 'A' when open, 'C' when close and
// 'R' when handling reset
// recover status in case of program restart: load status file 
// recover status in case of XRF restart: rewrite configuration
// reset output: 111 will reset output 
int powerBoxStatus(char op, struct ST *xrf)
{
FILE* ctf = fopen(CTFILE, op=='w'?"w":"r");
char line[50];
int ris = 0;

	if(ctf) {
		if(op == 'w')
			fprintf(ctf, "%4.3f %4.2f %d %*.*s %4.3f\n",
				xrf->light, xrf->temp, xrf->irOn, MAX_PIN, MAX_PIN, xrf->o, xrf->umidita);
		else {
			fgets(line, sizeof(line)-1, ctf);
			xrf->light = atof(strtok(line, " "));
			xrf->temp = atof(strtok(NULL, " "));
			xrf->irOn = atoi(strtok(NULL, " "));
			strncpy(xrf->o, strtok(NULL, " "), MAX_PIN);
		}
		fclose(ctf);
		ris = 1;
	}
	loggerX(3, "powerBoxStatus OP=%c esito=%d", op, ris);
	return ris;
}
void powerBox (char* buf) // command received
{
static struct ST *xrf = NULL; // pointer to ST struct of XRF devices
// previous status of 4 relay('0'/'1') + 1 valve ('A'=opening/'C'= closing/'0'=static)
static char oOld[MAX_PIN+1];
static short battC = -7; // countdown for battery check
static short anaC = -7; // countdown for ADC reading
register short ind;
char cmd[300], *pt;


	loggerX(3, "powerBox entering [a%s]", buf);
	buf+=2;
	if(xrf == NULL) {
		for(ind = 0; ind<XRF_MAX; ind++) {
			if(strncmp(st[ind].name, "CT", 2) == 0) { // found the device position
				xrf = &st[ind];
				break;
			}
		}
	}


	xrf->fails = 0; // message received, reset flag for waiting message
	memset(cmd, 0 ,sizeof(cmd));
	xrf->on = TRUE;
	xrf->msgGone = 0;
	battC++;


	if(CK_MSG(buf, "STARTED")) {
		// discard multiple STARTED sequence
		if((time(NULL) - xrf->timeLastAnsw) > 2) {
			if(CK_MSG(buf, "STARTED")) serialSend("CT", "ACK");

			strncpy(oOld, "11110A", MAX_PIN); // default configuration at restart of the PowerBox
											// valve==A to force close
			if(xrf->o[0] == '\0' ) { // restarted program also 
				if(!powerBoxStatus('r', xrf)) {
					loggerX(0, "powerBox Cannot open file %s setting default", CTFILE);
					strncpy(xrf->o, "11110C", MAX_PIN); // force Close valve
				}
			}
			if(pt = getCmd(NULL, oOld, xrf->o)) {
				strcpy(cmd, pt);
			}
		}
	}
	// if received HELLO the program restarted then it is valid the XRF status
	// if received REBOOT the XRF restarted then read PIC status
	else if(CK_MSG(buf, "HELLO") || CK_MSG(buf, "REBOOT")) {
		if(xrf->o[0] == '\0' ) { // restarted program
			strncpy(oOld, "11110A", MAX_PIN); // default configuration at restart of the PowerBox
											// valve==A to force close
			if(!powerBoxStatus('r', xrf)) {
				loggerX(0, "powerBox Cannot open file %s setting default", CTFILE);
				strncpy(xrf->o, "11110C", MAX_PIN); // force Close valve
			}
		}
		strcpy(cmd, "INPA");
	}
	else if(CK_MSG(buf, "BATT")) {
	float bt;

		sscanf(buf, "BATT%f-", &bt);
		xrf->batt = bt;

		battC = 0;
		strcpy(cmd, "ANAA");
	}
	else if(CK_MSG(buf, "INP")) {
	register short pnt = buf[3] - 'A';

		oOld[pnt] = buf[4];
		if(xrf->o[pnt]!='0' && xrf->o[pnt]!='1') xrf->o[pnt] = oOld[pnt];

		if(buf[3] != 'D')
			sprintf(cmd, "INP%c", (char)(buf[3]+1));
		else {
			if(battC < 0 || battC > 60)
				strcpy(cmd, "BATT");
		}
	}
	// read from XRF ADC
	else if(CK_MSG(buf, "ANA")) {
	register char *pt = &buf[4];

		switch(buf[3]) {
		// ANAA=luce, ANAB=temp, ANAC=ir, ANAD=umiditÃ 
		case 'A': xrf->light = atof(pt)/2047; strcpy(cmd, "ANAB"); break;

		case 'B': { const float m = (33.5-26.2)/(562-464); const float b = 26.2-m*464.0;
					float x = atof(pt);
					//xrf->temp = (v/anaT)*baseT + fabs(v)*(v-baseT)^2;
					xrf->temp = m*x+b;
					strcpy(cmd, "ANAC"); } break;

		case 'C': xrf->irOn = atoi(pt); strcpy(cmd, "ANAD"); break;

		case 'D': xrf->umidita = atof(pt)/2047;
				
			if(pt = getCmd(NULL, oOld, xrf->o)) {
				strcpy(cmd, pt);
			}
			break;
		}
	}
	// write internal data to XRF-PIC
	else if(CK_MSG(buf, "OUT")) {
		if(pt = getCmd(buf, oOld, xrf->o)) {
			strcpy(cmd, pt);
		}
		else {
			strcpy(cmd, "INPA");
		}
	}

	xrf->timeLastAnsw = time(NULL);
	if(cmd[0])
		serialSend("CT", cmd);
	else if(xrf->manualMsg[0]) {
		strcpy(cmd, xrf->manualMsg);
		memset(xrf->manualMsg, 0, sizeof(xrf->manualMsg));
		serialSend("CT", cmd);
	}
	else {
		if(xrf->irOn)
			printXRFStatus(xrf);
		signal(SIGALRM, CTXRFalarm);     // install the alarm handler for next polling
		if(!powerBoxStatus('w', xrf))
			loggerX(0, "powerBox Cannot open file %s for writing", CTFILE);
		alarm(xrf->sleepTot);
		if(battC%5 == 0) {
			XRFthingSpeak(xrf);
		}
	}
		
	loggerX(3, "powerBox exiting Sending=[%s]", cmd);
	return;
}
