# README #

Simple project WITHOUT makefile, developed on Raspbian, should complile only with
gcc -o gpmon  gpmon.c xrf.c util.c -l wiringPi -l m -l pthread -l curl -O3
In case to use with valgrind add to options also 
-g -rdynamic

### What is this repository for? ###

* gpmon is a general purpose monitor to supervision the tasks running on my server RPi, it run always but it is not completely configured as a daemon or service
* 2.00 with RFThermostat and CTPowerBox
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Setup
Simple create /opt/bin, copy gpmon and gpmon.conf in that folder

* Configuration
The configuration file is auto explained, the running option are quite simple
/opt/bin/gpmon <loglevel> <logfile>
loglevel 0-9, standard value should be 1
logfile typically /var/log/gpmon.log

* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact