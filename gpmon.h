#include <errno.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <wiringSerial.h>

// global definitions and variables
typedef unsigned char		bool;
#define RADIX "/dev/shm/" // folder for files
#define MODEMDEVICE "/dev/ttyAMA0"
#define PIDFILE "/var/run/gpmon.pid"
#define LUCEFLAG "/run/shm/luce"
#define WATERFLAG "/run/shm/acqua.flg"
#define SETTINGS "/opt/bin/gpmon.conf" // contains thermostat scheduling
#define POWFILE RADIX "power.dat" // contains levels of power supply
#define THFILE RADIX "thfile.db" // store temp and light indoor
#define CTFILE RADIX "ctfile.db" // store temp and light outdoor, pin status
#define IRFILE RADIX "irda.dat" // file with flag of IR activation indicator
#define MACSDB RADIX "netstat.db" // file with MAC address of other devices in the house
#define STATFILE RADIX "mobile.dat" // optimized last status of mobiles in house
#define DBFILE RADIX "mobile.db" // database of real presence in network of mobiles
#define BASH_ALIASES "/home/pi/.bash_aliases"
#define CAMFILE "RPi_camtime.txt"

#define MAX_RANGES 3 // valid periods in a day for thermostat and watering
#define MAX_PERIOD 20
#define FALSE 0
#define TRUE 1
#define XRF_MAX 10
#define MAX_TSFLDS  8 // maximum fields in every channel
#define THINGS_CH1_KEY "8GL51FPSEAAHQ2GL" // ThingSpeak channel 7091(Thermostat) key to upload data
#define THINGS_CH2_KEY "LYJWX4G395JICTO0" // ThingSpeak channel 19111(PowerBox) key to upload data
#define MCP23017_PORTA 0x12		// GPIOA (see data sheet)
#define MCP23017_PORTB 0x13		// GPIOB (see data sheet)
#define CHARS  76;	// Sending 54 chrs at a time with \r , \n and \0 it becomes 57 
#define I2CLUCE 5
#define I2CCASSE 3
#define I2CCDROM 6
#define I2CROUTER 1
#define I2CPOW 4
#define I2CWEBLIGHT 8


#define loggerI(level, fmt, ...) _logger(logOnI, level, fmt"\n", ##__VA_ARGS__)
#define loggerX(level, fmt, ...) _logger(logOnX, level, fmt"\n", ##__VA_ARGS__)
#define logger(level, fmt, ...) _logger(logOn, level, fmt"\n", ##__VA_ARGS__)
#define CK_MSG(a,b)	(strncasecmp(a,b,strlen(b))==0)

typedef struct {
	char *s;
	int l;
} string;
typedef struct {
	unsigned char st; // 0-no value, 1-valid int, 2-valid float
	union { int vi; float vf; } v;
	} TSFLDS_T; // ThingSpeak data structure to pass to thingSpeac function
typedef struct {
	int mesgio;
	int ora;
} ORALUCE; // setting for the starting lightning period in the dat

// struct for device status variables
#define MAX_PIN 6
struct ST {
	char	name[3]; // name of the XRF
	short	on;		 // XRF is active
	short	msgGone; // msg sent yet without answer
	int		fails;	 // counter for errors (after some amount restart is needed)
	time_t	timeLastSent;// XRF last comm sent
	time_t	timeLastAnsw;// XRF last comm received
	char	sleepTotStr[5]; // XRF will sleep these LLAP time (see SLEEP command)
	int		sleepTot;// XRF will sleep these LLAP time (value of field above)
	char 	o[MAX_PIN+1]; // XRF CT output pin value
	float	temp;	 // XRF detected temp
	float	ntc;	 // resistance value of NTC
	float	light;	 // XRF detected light level
	short	relayOn; // XRF relay active auto, 0 off, 1 on
	short	relayOnMan; // XRF relay active manually, 0 off, 1 on
	float	umidita; // XRF umidity level in the green
	float	batt;	 // XRF battery voltage
	short	irOn;	 // IR detection enabled
	time_t	mailTime;// time of last low battery message sent
	char	msg[15]; // XRF last message sent
	char	manualMsg[15]; // Message added by runtime input in console
	FILE*   cmd;
} st[XRF_MAX];


extern FILE *logF; // global variable for file logging
extern char *logFN; // global variable for file logging
extern int logOn; // general log level 0 none, 1 error, 2 info, 3 debug
extern int logOnX; // log level as above for XRF.c module
extern int logOnI; // log level as above for IRDA functions
extern struct CFGDATA{ // global conf variables
	struct {
		float temp;		// minimum temp value
		time_t start;
		time_t end;
	} t[7][MAX_RANGES]; // Heating data
	bool spento;		// Heater forced OFF if true
	struct {
		float moisture; // minimum level, below start watering
		time_t start;
		time_t stop;
	} i[7][MAX_RANGES];				// Watering data
	bool irriga;		// Watering ON if true
	ORALUCE ol[MAX_PERIOD]; // ORALUCE loaded from config file
	char* to;
	char* url;
	char* user;
	char* pass;
} cfg;


// prototypes
extern int thingSpeak(	const char	*key, TSFLDS_T	*data); // thingspeak pubblication function
extern char* upt(); // update the time string user to log message
extern char* byte2bin(unsigned char v);

// overload the fprintf to handle logging to file correctly
//  logT is the logType level
// the line below is useful for GCC complile time arguments checking
extern void _logger(int type, int level, char *fmt, ...) __attribute__((format (printf, 3, 4)));

extern bool I2Ccomm(int dev, unsigned char* wr, int nwr, unsigned char* rd, int nrd);
extern bool MCP23017(int port, bool read, int bit, bool on);


// standard function to format LLAP command, send it to the channel and log it if logging is enabled
extern void serialSend(const char *dev,	// device to send message
				const char *str);	// message to send, if NULL resend last message

// function threaded to drive all of the XRF
extern void *xrfmon(void *arg);
extern void *monSt(void *arg);	// thread for status control from monit
extern void IRdetect(void); // thread to handle interrupt from motion sensors
extern char* XRFcmd (char* buf);// used to manage commands for XRF radio modules 
extern char* printXRFStatus(struct ST* xrf); // print XRF status
