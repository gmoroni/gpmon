// Compile code with
// gcc -o gpmon  gpmon.c xrf.c util.c -l wiringPi -l m -l pthread -l curl -O3
// to debug add -g -rdynamic
// ThingSPeak API Write KEY 8GL51FPSEAAHQ2GL for channel 7091 and LYJWX4G395JICTO0 for 19111

#include <wiringPi.h>
#include "gpmon.h"
#include <curl/curl.h>
#include <execinfo.h>
#include <signal.h>
#include <time.h>


// global definitions and variables
#define LISTEN_PORT 51412 // port to listen for incoming instructions


#ifdef OK_MJPG
#define STOP_MJPG "sudo pkill -9 mjpg_streamer &"
#else
#define GET_IMG "fswebcam -r 1280x960 -F 1 --jpeg 95 -q /tmp/mjpg/webc%s.jpg"
#endif

const char* DBFMT = "%*.*s %02d%02d\n"; // formato record di STATFILE e DBFILE
#define NMACS	6 // Max number of MAC address to analize, see MACS array
#define MAC_LEN 17 // Length of a MAC address
int nmacs = 0; // loaded MACs from config file
char* MACS[NMACS];
char getImgCmd[400]; // command to start MJPG streamer taken from .bash_aliases
const int DELTA_SEC = 300; // length of webcam progra activation if no IRdetection
const short PIN_IRDA = 1; // pin connected IRDA
char People[NMACS]; // mobile presence in the house + personal PC
struct CFGDATA cfg; 

// structure to handle all of the global variables to manage alarms (and webcam)
struct IRDA {
	bool alarmEnabled; // alarm enabled or not
	bool webpEnabled; // by default webcam program is enabled
	bool webpOn; // webcam program active or not
	short minCamOn; // countdown minutes for dropbox activation
	time_t timeMotion; // instant of webcam program service activation, 0 webcam program is off
	time_t timeIRdetect; // instant of last IR detection
	int peopleOK; // number of known people in house
	bool alwaysOn; // if not zero it starts webcam program even with mobiles in house
	bool onlyMama; // if not zero only mama's mobile is monitored
	bool sendMail; // if on a mail should be sent
} irda;



// prototypes
void *monSt(void *arg);	// thread for status control from monit
int readSMS(char* dev); // read SMS and execute related command
int checkI2C(int dev); // check power levels through I2C ADC
void *mobileLight();



// detect if webcam program service is active and in that case assign time of detection
int webpStatus(void) {
FILE *cmd = popen("pidof -s mjpg_streamer", "r");
char line[200];

	if(cmd && (fgets(line, sizeof(line), cmd) == NULL)) {
		irda.timeMotion = 0;
		irda.webpOn = FALSE;
	}
	else {
		if(irda.timeMotion == 0)
			irda.timeMotion = time(NULL);
		irda.webpOn = TRUE;
	}
	pclose(cmd);
	return irda.webpOn;
}

void stackTrace(int sig) {
void *array[10];
size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(logF, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, fileno(logF));
  exit(1);
}


// This function checks for a Dropobox file Public/RPi_camtime.txt in folder Public in which there
// is a CAMERA counter file with activation minutes, return the file content value
// this function returns: <0 error, 0 file is empty or invalid, >0 content value
#define DROPBOXURL "https://api-content.dropbox.com/1/files/dropbox/%%2FPublic%%2F%s?oauth_consumer_key=vfitft5b7tlnhkz&oauth_token=hmjjhiofmve304ke&oauth_signature_method=PLAINTEXT&oauth_signature=49fm5ie9ccj70w6%%269xe1n1hmj69vlq4&oauth_timestamp=%010d&oauth_nonce=%05d"
int getDropboxVal(char* fileN) {
char buf[500];
int ris = -1;
string val;

	loggerI(3, "getDropboxVal entering file=%s", fileN);
	
	sprintf(buf, DROPBOXURL, fileN, time(NULL), rand()%99999);
	val.s = '\0';
	val.l = 0;

	if(curlOp(buf, NULL, 0, NULL, &val)) {
		loggerI(3, "getDropboxVal from web[%s]", val.s);
		ris = atoi(val.s);
	}
	else
		loggerI(1, "getDropboxVal URL error");

	loggerI(3, "getDropboxVal exiting [%s] ris=%d", buf, ris);
	return ris;
}
// this function returns:
// -1 cannot prepare local file
// -2 cannot upload file
// =0 file uploaed
int putDropboxVal(char* fileN, int val) {
char buf[500], nome[50];
FILE* pf;
int ris = -1;

	loggerI(3, "putDropboxVal entering file=%s val=%d", fileN, val);
	
	sprintf(nome, "/tmp/%s", fileN);
	sprintf(buf, DROPBOXURL, fileN, time(NULL), rand()%99999);

	if((pf = fopen(nome, "w+")) != NULL) {
		fprintf(pf, "%d\n", val);
		fseek(pf, 0L, SEEK_SET);

		ris = -2;

		if(curlOp(buf, NULL, 1, pf, NULL)) {
			loggerI(3, "putDropboxVal curlOp OK");
			ris = 0;
		}
		else
			loggerI(1, "putDropboxVal URL [%s] error", buf);
		fclose(pf);
		unlink(nome);
	}
	else {
		loggerI(0, "putDropboxVal cannot create %s file", nome);
		ris = -1;
	}

	loggerI(3, "putDropboxVal exiting ris=%d",ris);
	return ris;
}



int getSendImg(char* resp) 
{
int ris = 1;
char cmd[300];
register bool lowLight = 0;
register short i;
FILE* pimg;

	loggerI(3, "getSendImg entering");
	if(resp) *resp = 0;
	memset(cmd, 0, sizeof(cmd));
	// turn on light in case room with low light and prepare the off command
	for(i = 0; i < XRF_MAX; i++) {
		if(strncmp(st[i].name, "TH", 2) == 0) {
			if(st[i].light < 400) {
				lowLight = 1;
				if(!MCP23017(MCP23017_PORTA, 0, 8, 1))
					logger(0, "getSendImg webLight ON failed");
				break;
			}
		}
	}

	// try to take snapshot from other camera, if it fails no problems
	sprintf(cmd, "/tmp/mjpg/tenvis-%s.jpg", upt());
	if(pimg = fopen(cmd, "wb")) {
		if(curlOp("http://192.168.1.239:81/snapshot.cgi","admin:pwd=passwd00",0,pimg,NULL))
			fclose(pimg);
		else {
			fclose(pimg);
			unlink(cmd);
		}
	}
	else
		logger(1, "getSendImg error opening %s: %d-%s", cmd, errno, strerror(errno));
#ifdef OK_MJPG
	strcpy(cmd, getImgCmd);
#else
	sprintf(cmd, getImgCmd,  upt());
#endif
	if(system(cmd)) {
		logger(0, "getSendImg fail system(%s) %d-%s", cmd, errno, strerror(errno));
		ris = 0;
		if(resp) strcpy(resp, "failed");
	}
	else {
		ris = sendMail("IR ALARM - detected sending pictures");
		if(resp) strcpy(resp, "done");
	}

	if(lowLight && !MCP23017(MCP23017_PORTA, 0, 8, 0)) {
		logger(0, "getSendImg webLight OFF failed");
	}
		
	loggerI(3, "getSendImg exiting [%d%s%s]", ris);
	return ris;
}
int spegniCamera(char* resp)
{
}

void unlinkPid(void)
{
	unlink(PIDFILE);
}

int main(int argc, char** argv)
{
int fd, c, res, ind;
short help = FALSE;
char buf[400];
pthread_t thr1, thr2, thr3;
static char wBUF[100];

	sprintf(buf, "%d\n", getpid());
	if((fd = open(PIDFILE, O_WRONLY|O_CREAT)) < 0) {
		perror(PIDFILE);
		exit(-1);
	}
	else {
		write(fd, buf, strlen(buf));
		close(fd);
		atexit(unlinkPid);
	}
	//signal(SIGSEGV, stackTrace);   // install our handler
	srand(time(NULL));
	logOn = logOnX = logOnI = 0;
	irda.alarmEnabled = 1;
	irda.webpEnabled = TRUE;
	irda.minCamOn = 0;
	irda.timeIRdetect = 0;
	irda.alwaysOn = 0;
	irda.onlyMama = 0;
#ifdef OK_MJPG
	webpStatus();
#endif
	
	if (wiringPiSetup () != 0) {
		perror("wiringPi setup failed");
		exit (-1) ;
	}


	strcpy(buf, "Starting");
	for(c = 0, ind=strlen(buf); c < argc; c++) {
		res = snprintf(&buf[ind], sizeof(buf) - ind, " [%s]", argv[c]);
		ind += res;
	}

	argv++; argc--;
	if(argv[0] != NULL && argc > 0) {

		
		if(argv[0] != NULL && strcmp(argv[0], "-A") == 0) {
			irda.alwaysOn = 1;
			argv++; argc--;
		}
		if(argv[0] != NULL && strcmp(argv[0], "-h") == 0) {
			help = TRUE;
			argv++; argc--;
		}
		if(argv[0] != NULL && strcmp(argv[0], "-P") == 0) {
			irda.onlyMama = 1;
			argv++; argc--;
		}
		if(argv[0] != NULL && strcmp(argv[0], "-I") == 0) {
			if(irda.onlyMama || irda.alwaysOn)
				help = TRUE;
			else
				irda.webpEnabled = FALSE;
			irda.onlyMama = 1;
			argv++; argc--;
		}
		if(argv[0] != NULL && isdigit(argv[0][0])) {
			logOn = logOnX = logOnI = atoi(argv[0]);
			argv++; argc--;
		}
		if(argv[0] != NULL) {
			logF = fopen(argv[0], "a+");
			// this limits the buffer for logging
			setvbuf( logF, wBUF, _IOFBF, (size_t)50 );
			logFN = argv[0];
			argv++; argc--;
		}
		if(argc > 1)
			help = TRUE;
	}
	if(help) {
		perror("Syntax is: gpmon [[-A] | [-P] | [-I]] [loglevel] [logfile]\n");
		perror("where -A means start webcam program in every condition (light or people)\n");
		perror("      -P means start webcam program only if papa' is not in house\n");
		perror("      -I disable checking of movements to enable webcam program\n");
		perror("      loglevel is the level of logging in logfile, if not specified use stdout\n");
		exit(-1);
	}
	if(logF == NULL) {
		logF = fdopen(0, "a+");
	}
	if(logF == NULL) {
		perror(argc == 1? "error opening STDOUT" : "error opening the logging file");
		exit(-1);
	}
	fprintf(logF,"\n\n===========================================================\nmain [%s]", buf);
	logger(0, "main alwaysOn=%d help=%d onlyMama=%d logOn=%d,%d,%d logname=[%s]",
				irda.alwaysOn, help, irda.onlyMama, logOn, logOnI, logOnX, logFN);
	
	if(!loadConfFile(SETTINGS)) {
		logger(0, "main Error in loading configuration file");
		exit(1);
	}
#ifdef OK_MJPG
	bash = fopen(BASH_ALIASES, "r");
	if(bash) {
	register char *pt;

		while(fgets(buf, sizeof(buf)-1, bash) != NULL) {
			if((pt = strstr(buf, "cam='")) != NULL) {
				pt += strlen("cam='");
				strncpy(getImgCmd, pt, strlen(pt)-2);
				strcat(getImgCmd, " 2>/dev/null &");
				continue;
			}
		}
		fclose(bash);
	}
#else
	sprintf(getImgCmd, "%s 2>/dev/null", GET_IMG);
#endif
	logger(1, "getImgCmd=[%s]", getImgCmd);

	// activation of the status thread 
	if(pthread_create(&thr1, NULL, monSt, NULL)) {
		perror("ERROR in monst thread creation");
		exit(1);
	}
	else {
		pthread_detach(thr1);

		// activation of the xrfmon thread 
		if(pthread_create(&thr2, NULL, xrfmon, NULL)) {
			sprintf(buf, "ERROR in xrfmon thread creation");
			perror(buf);
			exit(1);
		}
		else
			pthread_detach(thr2);
	}


	// assign the function to handle the IR detection of the webcam program sensor, physical pin 12 (wiringPi 1)
	if(irda.webpEnabled) {
		if ( wiringPiISR (PIN_IRDA, INT_EDGE_RISING, &IRdetect) < 0 ) {
			perror("ERROR in interrupt setting for IRdetect");
			exit(1);
		}
	}
	irda.sendMail = 0;

	// configuration of CURL library
	curl_global_init(CURL_GLOBAL_ALL);

	// this cycle manage deactivation of webcam program service after a DELTA_SEC time
	unsigned short contaCicli = 0;
	const unsigned short MINUTI = 2; // minuti tra due get su Dropbox
	const unsigned short SECONDI = 10; // durata dell'attesa nel ciclo
	int timing = 21; // numero di cicli tra due letture per ADC
	while(TRUE) {
	FILE *fh;

		contaCicli%=65534;

		// check bit 1 for router relay and re-enable if necessary (logic 1 is off)
		// VVERY CRITICAL CODE, IF WRONG LOGIC WILL TURN OFF ROUTER
		if(!(contaCicli%6) && MCP23017(MCP23017_PORTA, 1, I2CROUTER, 0) != 0) {
			logger(0, "==> Reactivating Router Relay <==");
			MCP23017(MCP23017_PORTA, 0, I2CROUTER, 0);
		}

		// activation of the mobileLight thread every 10 minutes
		if(!(contaCicli%60)) {
			if(pthread_create(&thr3, NULL, mobileLight, NULL)) {
				logger(0, "main pthread_create(mobileLight) failed: %s", strerror(errno));
				sleep(2);
				if(pthread_create(&thr3, NULL, mobileLight, NULL)) {
					logger(0, "main pthread_create(mobileLight) failed twice: %s", strerror(errno));
					exit(0);
				}
				else
					pthread_detach(thr3);
			}
			else
				pthread_detach(thr3);
		}


		// every 10 cycles check SMS queue
		if(!(contaCicli%10)) readSMS("/dev/ttyUSB0");

		// every 21 cycles check I2C ADC to see battery level
		if(!(contaCicli%timing)) timing = checkI2C(0x34);

		// ogni 60 minuti verifica lo stato del file su Dropbox per attivare o no la telecamera
		if(irda.alarmEnabled && !(contaCicli%(MINUTI*60/SECONDI))) {

			loggerI(3, "Dropbox file %s %d", CAMFILE, irda.minCamOn);
			// se la variabile e' vuota provo a vedere su Dropbox
			if(irda.minCamOn == 0) {
				irda.minCamOn = getDropboxVal(CAMFILE);
				if(irda.minCamOn <= 0)
					irda.minCamOn = 0;
				else {
#ifdef OK_MJPG
					if(!getSendImg(buf))
   						loggerI(0, "main getSendImg %s", buf);
#endif
				}
			}
			else { // aggiorno il contatore dei minuti anche su Dropbox
				irda.minCamOn-=MINUTI;
				if(irda.minCamOn < 0) irda.minCamOn = 0;
				putDropboxVal(CAMFILE, irda.minCamOn);
				if(irda.minCamOn == 0) {
#ifdef OK_MJPG
					spegniCamera(buf);
#else
					buf[0] = 0;
#endif
					loggerI(1, "Dropbox file cleaned, webcam off %s", buf);
				}
			}
		}

		if((fh = fopen(IRFILE, "w")) != NULL) {
			fprintf(fh, "%d", digitalRead(PIN_IRDA));
			fclose(fh);
			fh = NULL;
		}
#if OK_MJPG
		if(irda.alarmEnabled && irda.webpEnabled && irda.minCamOn == 0) {
		int delta;

			webpStatus();
			delta = time(NULL) - irda.timeIRdetect;
			if(irda.webpOn && irda.timeIRdetect !=0 // if webcam program is on from gpmon
			&& delta > DELTA_SEC) {
				if(!irda.alwaysOn && system(STOP_MJPG) < 0) {
					loggerI(3, "main OFF webcam failure %d-%s", errno, strerror(errno));
				}
				else {
					loggerI(3, "main OFF webcam delta=%d > %d", delta, DELTA_SEC);
					webpStatus(); // update variable values accroding to status
					irda.sendMail = 0; // no more mail to send
				}
			}
		}
#endif

		// check if logfile is deleted or truncated and reopen it for append
		sleep(SECONDI);
		contaCicli++;
	}
}


// convert a hh:mm or hhmm to a time_t value (seconds)
static inline time_t a2sec(char* str) {
register time_t ris = 0;
register int len = 0;

	if(*str == ' ') *str = '0'; // if 1st char is blank change it to 0
	len = strspn(str, "01234567890:");
	if(len < 3 || len > 5) return 0;

	if(str[1] == ':')
		ris = (str[0] - '0')*3600;
	else
		ris = (str[0] - '0')*36000+(str[1] - '0')*3600;
	if(str[2] == ':')
		ris += (str[3] - '0')*600+(str[4] - '0')*60;
	else
		ris += (str[2] - '0')*600+(str[3] - '0')*60;
	return ris;
}

// load configuration file
int loadConfFile(const char* nome) {
FILE* fo;
char line[400], campo[15], *pt, *pt2;
register int ind, day, lineC;


	logger(3, "loadConfFile loading file [%s]", nome);
	if((fo = fopen(nome, "r")) == NULL) {
		logger(0, "loadConfFile file=%s errno=%d-%s", SETTINGS, errno, strerror(errno));
		return 0;
	}
	lineC = 0;
#define READ_LINE(a) while(fgets(a, sizeof(a)-1, fo) && ++lineC && (a[0] == '#' || a[0] == ';'))
	while(TRUE) {
		READ_LINE(line); if(feof(fo)) break;

		if(CK_MSG(line, "TERMOSTATO")) {

			if(strcasestr(line, "OFF"))
				cfg.spento = 1;
			continue;
		}
		if(CK_MSG(line, "DOMENICA")) {
			ind = 0;
			// each field is "starttime,endtime,temp":time is 4 digits, temp is 2 digits integer
			do {
				READ_LINE(line); if(feof(fo)) break;

				for(day = 0, pt2 = line; day < 7; day++) {
					pt = strsep(&pt2, "|");
					if(pt != NULL) strcpy(campo, pt);
					else continue;

					if(strcspn(campo, " ") != 12) {
						continue;
					}
					else {
						cfg.t[day][ind].start = atoi(strtok(campo, ","));
						if((pt = strtok(NULL, ",")) != NULL) 
							cfg.t[day][ind].end = atoi(pt);
						if((pt = strtok(NULL, ",")) != NULL)
							cfg.t[day][ind].temp = atoi(pt);
						logger(1, "loadConfFile wday=%d t=%f start-end %d-%d",
								day, cfg.t[day][ind].temp, cfg.t[day][ind].start, cfg.t[day][ind].end);
					}
				}
				ind++;
			} while(ind > 0 && ind < MAX_RANGES);
			continue;
		}
		
		//IRRIGA=ON 1,2,3,4,5,6,7 6:00-5-0.5,23:00-5-0.5
		if(strcasestr(line, "IRRIGA=")) {
		char* pt3, lineBck[150];
		time_t s[2]= {0,0} ,e[2]= {0,0};
		float v[2]= {0,0};

			if(strcasestr(line, "OFF")) continue;

			cfg.irriga = 1;
			strcpy(lineBck, line);
			pt2 = strstr(line, " "); while(*pt2 == ' ') pt2++; // begin of 2nd field
			pt3 = strstr(pt2, " "); *(pt3++) = 0; while(*pt3 == ' ') pt3++; // 3rd field
			if(pt2 && pt3) { 
				for(ind = 0; ind < 2; ind++) { // analize 3rd field
					if((pt = strtok_r(pt3, "-", &pt3)) == NULL) {
						if(ind == 0) {
							logger(0, "loadConfFile error in line [%s]", lineBck);
							exit(1);
						}
						else
							break;
					}
					s[ind] = a2sec(pt);
					if((pt = strtok_r(NULL, "-", &pt3)) == NULL) {
						logger(0, "loadConfFile error in line [%s]", lineBck);
						exit(1);
					}
					e[ind] = s[ind] + atoi(pt)*60;
					if((pt = strtok_r(NULL, "-,", &pt3)) == NULL) {
						logger(0, "loadConfFile error in line [%s]", lineBck);
						exit(1);
					}
					v[ind] = atof(pt);
				}
				
				for(pt = strsep(&pt2, ","); pt; pt = strsep(&pt2, ",")) {
					day = atoi(pt)-1;
					if(day < 7) {
						cfg.i[day][0].start = s[0]; cfg.i[day][1].start = s[1];
						cfg.i[day][0].stop = e[0]; cfg.i[day][1].stop = e[1];
						cfg.i[day][0].moisture = v[0]; cfg.i[day][1].moisture = v[1];
					}
					else {
						logger(0, "loadConfFile error in line [%s]", lineBck);
						exit(1);
					}
				}
			}
			else {
				logger(0, "loadConfFile error in line [%s]", lineBck);
				exit(1);
			}
			// prepare printout of loaded values
			for(pt = line, day = 0; day < 7; day++) {
			char *gg[7] = {"lun","mar", "mer","gio","ven","sab","dom" };
				if(cfg.i[day][0].start) {
					pt += sprintf(pt, "%s,", gg[day]);
				}
			}
			*(pt-1)=0;
			logger(1, "loadConfFile IRRIGA=%s %5d-%5d se < %4.3f e  %5d-%5d se < %4.3f il %s",
				cfg.irriga?"ON":"OFF", s[0], e[0], v[0], s[1], e[1], v[1], line);
			continue;
		}
		if(strcasestr(line, "TO=")) {
			pt = line + 3;
			cfg.to = malloc(strlen(pt)+1);
			strcpy(cfg.to, pt);
			continue;
		}
		if(strcasestr(line, "SMTP=")) {
			pt = line + 5;
			cfg.url = malloc(strlen(pt)+1);
			strcpy(cfg.url, pt);
			continue;
		}
		if(strcasestr(line, "USER=")) {
			pt = line + 5;
			cfg.user = malloc(strlen(pt)+1);
			strcpy(cfg.user, pt);
			continue;
		}
		if(strcasestr(line, "PASS=")) {
			pt = line + 5;
			cfg.pass = malloc(strlen(pt)+1);
			strcpy(cfg.pass, pt);
			continue;
		}
		if(strcasestr(line, "MACS=")) {
			for(ind = 0, pt = strtok(line + 5, ";");
				pt && ind < NMACS;
				ind++, pt = strtok(NULL, ";")) {
				MACS[ind] = calloc(MAC_LEN+1, sizeof(char));
				strncpy(MACS[ind], pt, MAC_LEN);
			}
			nmacs = ind;
			for(ind = 0, pt = line; ind < nmacs; ind++)
				pt += sprintf(pt, "%s ", MACS[ind]);
			logger(1, "loadConfFile NMACS=%d:\n%s", nmacs, line);
			continue;
		}
		if(line[0] == '[') { // load light-time data, each token is [101]=1730 or [1202]=1630
			for(ind = 0, pt = strtok(line, " ");
				pt != NULL && ind < sizeof(cfg.ol)/sizeof(ORALUCE);
				pt = strtok(NULL, " "),  ind++) {
				pt++;
				cfg.ol[ind].mesgio = atoi(pt);
				pt=strstr(pt, "="); pt++;
				cfg.ol[ind].ora = atoi(pt);
			}
			if(ind == sizeof(cfg.ol)/sizeof(ORALUCE)) {
				logger(0, "loadConfFile ligth-tempo data out of bounds, skipped from %d pos.", ind);
				fclose(fo);
				return 0;
			}
			for(ind = 0, pt = line; cfg.ol[ind].mesgio != 0; ind++)
				pt += sprintf(pt, "%4d=%d ", cfg.ol[ind].mesgio, cfg.ol[ind].ora);
			logger(1, "loadConfFile loaded this data:\n%s", line);
			continue;
		}
		logger(0, "loadConfFile line[%s] not understood, exiting");
		exit(1);
	}
	fclose(fo);
	logger(3, "loadConfFile exiting [0]");
	return 1;
}

// POST to upload data to thingSpeak, channel has a key as below
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

// catch SIGPIPE signal
int socket_closed;
void sigpipe_handler()
{
    socket_closed=1;
}

// console commands handling
int monOptions(char* buf, int sock) {
char outp[1000];
int ris = 1;

	logger(3, "monOptions enter [%s] %d", buf, sock);

	memset(outp, sizeof(outp), 0);
	if(CK_MSG(buf, "exit")) {
		sprintf(outp, "Command [%s] accepted\n", buf);
		ris = -1;
	}
	else if(CK_MSG(buf, "getfile")) {
		sprintf(outp, "Command [%s] running ... ", buf);
		send(sock, outp, strlen(outp), 0);
		sprintf(outp, "ended with value %d\n", getDropboxVal(CAMFILE));
	}
	else if(CK_MSG(buf, "camera")) {

		if(strcasestr(buf, "ON")) {
			sprintf(outp, "Command [%s] running ... ", buf);
			send(sock, outp, strlen(outp), 0);
			ris = getSendImg(outp);
			strcat(outp, "\n");
		}
#ifdef OK_MJPG
		else {
			sprintf(outp, "Command [%s] accepted\n", buf);
			system(STOP_MJPG);
		}
#endif
	}
	else if(CK_MSG(buf, "luce")) {
	register bool si;

		if(strstr(buf, "-"))
			si = MCP23017(MCP23017_PORTA, 1, I2CLUCE, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", !MCP23017(MCP23017_PORTA, 1, I2CLUCE, 0));
		else if(MCP23017(MCP23017_PORTA, 0, I2CLUCE, si ? 0:1)) {
		int fd;
		char nome[50];
			sprintf(nome, "%s%s", LUCEFLAG, si ? "no.flg" : "si.flg");
			unlink(nome);
			sprintf(nome, "%s%s", LUCEFLAG, si ? "si.flg" : "no.flg");
			if((fd = creat(nome, S_IRWXU|S_IRWXG|S_IRWXO)) > 0) {
				close(fd);
				sprintf(outp, "%d\n", si);
			}
			else
				sprintf(outp, "%d\n", !si);
		}
		else
			sprintf(outp, "%d\n", !si);
	}
	else if(CK_MSG(buf, "cdrom")) {
	register bool si;

		if(strstr(buf, "-"))
			si = MCP23017(MCP23017_PORTA, 1, I2CCDROM, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", !MCP23017(MCP23017_PORTA, 1, I2CCDROM, 0));
		else
			sprintf(outp, "%d\n",	MCP23017(MCP23017_PORTA, 0, I2CCDROM, si ? 0:1) ? si : !si);
	}
	else if(CK_MSG(buf, "casse")) {
	register bool si;

		if(strstr(buf, "-"))
			si = MCP23017(MCP23017_PORTA, 1, I2CCASSE, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", !MCP23017(MCP23017_PORTA, 1, I2CCASSE, 0));
		else
			sprintf(outp, "%d\n", MCP23017(MCP23017_PORTA, 0, I2CCASSE, si ? 0:1) ? si : !si);
	}
	else if(CK_MSG(buf, "router")) {
	register bool si;

		if(strstr(buf, "-"))
			si = MCP23017(MCP23017_PORTA, 1, I2CROUTER, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", !MCP23017(MCP23017_PORTA, 1, I2CROUTER, 0));
		else
			sprintf(outp, "%d\n", MCP23017(MCP23017_PORTA, 0, I2CROUTER, si ? 0:1) ? si : !si);
	}
	else if(CK_MSG(buf, "pow")) {
	register bool si;

		if(strstr(buf, "-"))
			si = MCP23017(MCP23017_PORTA, 1, I2CPOW, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", !MCP23017(MCP23017_PORTA, 1, I2CPOW, 0));
		else
			sprintf(outp, "%d\n", MCP23017(MCP23017_PORTA, 0, I2CPOW, si ? 0:1) ? si : !si);
	}
	else if(CK_MSG(buf, "weblight")) {
	register bool si;

		if(strstr(buf, "-"))
			si = !MCP23017(MCP23017_PORTA, 1, I2CWEBLIGHT, 0);
		else
			si = strcasestr(buf, "SI") ? 1 : 0;

		if(strstr(buf, "?")) 
			sprintf(outp, "%d\n", MCP23017(MCP23017_PORTA, 1, I2CWEBLIGHT, 0));
		else
			sprintf(outp, "%d\n", MCP23017(MCP23017_PORTA, 0, I2CWEBLIGHT, si ? 1:0) ? si : !si);
	}
	else if(CK_MSG(buf, "always")) {
		irda.alwaysOn = strcasestr(buf, "ON") ? 1:0;
		sprintf(outp, "Command [%s] accepted\n", buf);
	}
	else if(CK_MSG(buf, "alarm")) {
		irda.alarmEnabled = strcasestr(buf, "ON") ? 1:0;
		sprintf(outp, "Command [%s] accepted\n", buf);
	}
	else if(CK_MSG(buf,"log")) {
	int level = atoi(&buf[strcspn(buf, "0123456789")]);

		ris = 0;
		switch(toupper(buf[3])) {
		case 'I': logOnI = level; ris = 1; break;
		case 'X': logOnX = level; ris = 1; break;
		case 'G': logOn = level; ris = 1; break;
		case '*': logOn = logOnI = logOnX = level; ris = 1; break;
		}
		sprintf(outp, "Command [%s] %s\n", buf, ris?"accepted":"refused");
	}
	else if (CK_MSG(buf, "temp")) {
	float t;
	register int j;

		t = strtof(&buf[strlen("temp ")], NULL);
		if(errno == ERANGE)
			ris = 0;
		else {
			for(j = 0; j<XRF_MAX; j++) {
				if(strcasecmp(st[j].name, "TH") == 0) { // found the device position
					st[j].temp = t;
					break;
				}
			}
			if(j == XRF_MAX) ris = 0;
		}
		sprintf(outp, "Command [%s] %s\n", buf, ris?"accepted":"refused");
	}
	else if (CK_MSG(buf, "irda")) {
		sprintf(outp, "Command [%s] running ... ", buf);
		send(sock, outp, strlen(outp), 0);
		IRdetect();
		strcpy(outp, "completed\n");
	}
	else if (CK_MSG(buf, "readI2C")) {
	char rd[4], wr[4];
		
		wr[0] = MCP23017_PORTA;
		if(I2Ccomm(0x20, wr, 1, rd, 1))
			sprintf(outp, "Command accepted and executed %x\n", rd[0]);
		else {
			sprintf(outp, "Command accepted but failed\n");
			ris = 0;
		}
	}
	else if (CK_MSG(buf, "CT") || CK_MSG(buf, "TH") || CK_MSG(buf, "--")) {
		sprintf(outp, "%s\n", XRFcmd(buf));
	}
	else if (CK_MSG(buf, "?") || CK_MSG(buf, "help")) {
		strcpy(outp, "exit - terminate program\n\
log{I|X|G|*}{ |=}{level} - enable log level\n\
{XRF} wr <file> - enable/disable writing commands to <file>\n\
alarm{on|off} - turn ON/OFF the IRDA alarm\n\
camera{on|off} - turn ON/OFF the camera\n\
{luce|casse|pow|cdrom|weblight|router}[ ]{si|no|?|-} - device set ON|set OFF|queried|toggled\n\
always{on|off} - enable/disable permanent IRDA detection\n\
TH[ ]rel[ ]{on|off} - force ON/OFF TH relay\n\
{XRF}[ ]{HELLO|REBOOT|BATT|CHDEVID{xx}} - send the LLAP command to the XRF\n\
==>>  PIN_A=220V-A PIN_B=220V-B PIN_C=9V PIN_D=12V  <<==\n\
CT[ ]pin{A|B|C|D|E}{0|1} - turn ON/OFF each relay of the power box\n\
CT[ ]pinF{A|C} - open(A) close(C) the gardening valve\n\
CT[ ]RESET - reset relay to off value\n\
CT[ ]INP - read status of each relay of the power box\n\
temp {float} - set temperature level to float value\n\
irda - simulate a infrared signal detection\n\
getfile - download RPi_camtime.txt from Dropbox\n\
{?|help} - help menu (this screen)\n\
any other key gives the status\n");
	}
	else {
		sprintf(outp, "System:alwaysOn=%d onlyMama=%d logG,I,X=%d,%d,%d CAMERA %s since %s",
			irda.alwaysOn, irda.onlyMama, logOn, logOnI, logOnX, irda.webpOn ? "ON": "OFF", 
			ctime(&(irda.timeIRdetect)));
		strcat(outp, printXRFStatus(NULL));
		strcat(outp, "\n");
	}
	if (outp[0]) {
		send(sock, outp, strlen(outp), 0);
		logger(3, "monOptions %s [%s] command ris=%d", ris == 0 ? "refused" : "accepted", buf, ris);
	}
	return ris;
}


// thread for status management from monit and manual requests
// listen for two sockets max and wait for any chars to print XRF status
// at maximum frequency of 10 seconds 
// accepted commands (case insenitive) to control XRFs
// exit - exit from program
// THrelON - turn ON relay of TH manually
// THrelOFF - turn OFF relay of TH manually
void *monSt(void *arg) {
#define NSOCK 4 // maximum number of socket to handle plus listening one
int listener, maxFD; // listening socket, maximum number for open socket
fd_set bckSet; //sockets array for listening, monit and manual sockets
int val;
struct sockaddr_in temp;
char buf[20*XRF_MAX];

	logger(3, "monSt entering");
restart:
	// socket creation and configuration
	if((listener=socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        logger(0, "monSt socket error %d-%s", errno, strerror(errno));
#ifdef OK_MJPG
		system(STOP_MJPG);
#endif
		exit(1);
	}
	// set SO_REUSEADDR on sock to true (1), this is to reuase a TIME_WAIT socket
	val = TRUE;
	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	val = fcntl(listener,F_GETFL,0);
	fcntl(listener, F_SETFL, val | O_NONBLOCK);

	temp.sin_family=AF_INET;
	temp.sin_addr.s_addr=INADDR_ANY;
	temp.sin_port=htons(LISTEN_PORT);
	if(bind(listener,(struct sockaddr*)&temp, sizeof(temp)) < 0) {
	char cmd[300];

        logger(0, "monSt bind error port %d %d-%s", LISTEN_PORT, errno, strerror(errno));
		sprintf(cmd, "sudo kill -9 $(sudo netstat -anp|grep %d|head -n 1|tr -s ' \t' '/'|cut -f7 -d'/')",
						LISTEN_PORT);
		system(cmd);
		if(bind(listener,(struct sockaddr*)&temp, sizeof(temp)) < 0) {
        	logger(0, "monSt re-bind error %d-%s", errno, strerror(errno));
#ifdef OK_MJPG
			system(STOP_MJPG);
#endif
			exit(1);
		}
		else
        	logger(1, "monSt re-bind ok continue on listening");
	}

	if(listen(listener, 7) < 0) {
		logger(0, "monSt listen error %d-%s", errno, strerror(errno));
#ifdef OK_MJPG
		system(STOP_MJPG);
#endif
		exit(1);
	}
    // instal sigpipe handler
    signal(SIGPIPE, sigpipe_handler);

	logger(1, "monSt is awaiting connections");

	FD_ZERO(&bckSet);
	FD_SET(listener, &bckSet);
	maxFD=listener;
	while (TRUE) {
	register int sd, j;
	fd_set tmpSet;
	int tmpS;

		tmpSet = bckSet;
		val = select(maxFD+1, &tmpSet, NULL, NULL, NULL);
		if(val < 0) {
			logger(0, "monSt select error %d-%s", errno, strerror(errno));
			exit(1);
		}
		else if(val == 0) { // timeout
			sleep(1);
			continue;
		}
		for(sd = 0; sd <= maxFD; sd++) {
			if(!FD_ISSET(sd, &tmpSet))
				continue;


			// if sd is equal to listener is a new socket request
			// otherwise is a data on an older socket
			if(sd == listener) {
				tmpS = accept(listener, NULL, NULL);
				if(tmpS < 0) {
					if(errno == EAGAIN || errno == EWOULDBLOCK)
						continue;
					logger(1, "monSt accept error %d-%s", errno, strerror(errno));
					sleep(5);
					exit(1);
				}
				else {
					val = fcntl(listener,F_GETFL,0);
					fcntl(listener, F_SETFL, val | O_NONBLOCK);
					logger(3, "monSt new socket fd=%d", tmpS);
					FD_SET(tmpS, &bckSet);	
					if(tmpS > maxFD)
						maxFD = tmpS;
				}
			}
			else
				tmpS = sd;

			// recv is non blocking, therefore must be done a check on errno
			// to understand the situation
			val = recv(tmpS, buf, sizeof(buf)-1, 0);
			if(val < 0 || socket_closed) {
				if(errno == EAGAIN || errno == EWOULDBLOCK)
					continue;
				logger(0, "monSt recv error sock=%d %s", tmpS, strerror(errno));
				if(tmpS != listener) {
					close(tmpS);
				}
				FD_CLR(tmpS, &bckSet);
				continue;
			}
			else if(val > 0) {
				buf[val] = 0;
				val = strcspn(buf, "\n\r");
				buf[val] = 0;
				if(strlen(buf)>0) logger(1, "monSt received data [%s]", buf);

				if(monOptions(buf, tmpS) < 0) {
#ifdef OK_MJPG
					system(STOP_MJPG);
#endif
					exit(1);
				}
			}
			else {
				logger(3, "monSt close sock=%d: %s", tmpS, strerror(errno));
				if(tmpS != listener) {
					close(tmpS);
				}
				FD_CLR(tmpS, &bckSet);
			}
			logger(4, "monSt Cycling =========================================");
		}
	}
	return NULL;
}


// routine to handle gpio interrupt for IR detection
// then start webcam program if there are no home mobile in house and if there is enough light
void IRdetect(void)
{
int ris = 0;
struct tm now;
register int i;


	irda.timeIRdetect = time(NULL);
	localtime_r(&irda.timeIRdetect, &now);
	loggerI(3, "IRdetect entering");

	if(!irda.alarmEnabled)
		ris = 1;
	else if(irda.webpEnabled
		&& (irda.timeMotion == 0 || ((time(NULL) - irda.timeMotion) > DELTA_SEC))) {

		// people in the house: nmacs MAC address to analize
		irda.peopleOK = 0;
		for(i = 0; i < nmacs && irda.peopleOK == 0; i++) {
			if(People[i] == 0) { // this is to avoid launch alarm when not yet tested
				irda.peopleOK = 1;
				break;
			}

			if(People[i] == '1')
				irda.peopleOK = 1;
		}

		loggerI(3, "IRdetect People=(%*.*s) peopleOK=%d", nmacs, nmacs, People, irda.peopleOK);

		if(irda.alwaysOn == 1 || irda.peopleOK == 0) {
			if(!getSendImg(NULL)) {
				loggerI(0, "IRdetect error in camera activation");
			}
			else {
				loggerI(1,"IRdetect starting webcam program (mobile.dat=%*.*s) peopleOK=%d",
						nmacs, nmacs, People, irda.peopleOK);
				irda.timeMotion = time(NULL);
				irda.sendMail = 0;
			}
		}
	}
	loggerI(3, "IRdetect exiting ris=%d", ris);
}

#define _XOPEN_SOURCE
#include <ctype.h>

// read the response of the modem until OK\r included
int modemOut(int fd, char* buf, int size)
{
register char* pt;
register char* line;
register signed char c;
bool inString = 0;


	logger(3, "modemOut entering");
	memset(buf, 0, size);
	line = pt = buf;
	do {
		c = serialGetchar(fd);
		if(c < 0)
			break;
		else if(!isprint(c) && inString) { // withing quotes strange chars converted to .
				c ='.';
		}
		*pt = c; pt++;

		if(c=='"' && (*(pt-2) != '\\')) { // if " not quoted, then enter/exit a string
			inString =~inString;
			pt--; // strip off quotes to avoid special handling for quoted strings
		}
		else if(c == '\n')
		{
			if(line[0] == '\n') // discard empty lines
				pt = line;
			else if(strstr(line, "OK") != NULL) // last line
			{
				pt--; // keep only \r as EoR
				break;
			}
		}
		else if(c == '\r') // strip \r and keep \n as EoR
			pt--;
	} while ((pt-buf) < size);
	*pt = 0;

	logger(3, "modemOut exiting %d", pt-buf);
	return pt-buf;
}

// function to access modem device and seek for SMS with commands
// -1 is an error, 0 nothing to do, n number of succesfully executed commands
int readSMS(char* dev)
{
struct MSG {
	int num;
	int valid;
	time_t data;
	char text[50];
} msg[20];
static time_t errore = 0; // use to control loggin of error for no device
int msgCnt;
int mod;
register char* ptR;
register char* ptF;
char* msgR;
char* msgF;
const char* RS = "\n"; // record separator
const char* FS = ","; // field separator
int ok = -1;
char buf[3000];
int len;


	logger(3, "readSMS entering");

	memset(buf, 0, sizeof(buf));

	/* open the serial port of modem to be non-blocking (read will return immediatly) */
	mod = serialOpen(dev, 115200);
	if (mod > 0) {
		errore = 0;

		serialPuts(mod, "ATE0\r");
		modemOut(mod, buf, 1000);
		serialPuts(mod, "AT+CMGL=\"ALL\"\r");
		len = modemOut(mod, buf, sizeof(buf));
		
		memset(&msg[0], 0, sizeof(msg));
		// Message format line is made of two records:
		// +CMGL: 0,"REC UNREAD","+393385031168",,"14/10/30,23:52:31+04"
		// MESSAGE
		for(msgCnt = 0, ptR = strtok_r(buf, RS, &msgR);
			ptR != NULL && strstr(ptR, "OK") != NULL;
			ptR = strtok_r(NULL, RS, &msgR))
		{
		struct tm tm;
		char tmp[30];

			ptF = strtok_r(ptR, FS, &msgF);
			msg[msgCnt].num = atoi(ptF  + 6);
			ptF = strtok_r(NULL, FS, &msgF);
			ptF = strtok_r(NULL, FS, &msgF);
			if(strstr(ptF, "393385031168") != 0) msg[msgCnt].valid = 1;
			strcpy(tmp, strtok_r(NULL, FS, &msgF));
			strcat(tmp, " ");
			strcat(tmp, strtok_r(NULL, FS, &msgF));
			strptime(tmp, "%y/%m/%d %H:%M:%S", &tm);
			msg[msgCnt].data = mktime(&tm);
			strncpy(msg[msgCnt].text, strtok_r(NULL, RS, &msgR), sizeof(msg[msgCnt].text)-1);
			if(msg[msgCnt].valid) msgCnt++;
		}
		logger(3, "readSMS loaded %d SMS", msgCnt);
	
		ok = 0;
		if(msgCnt > 0) {
			ok = 1;

			fflush(logF);
			fsync(fileno(logF));
			// Execute the received commands
			for(msgCnt = 0; msg[msgCnt].valid; msgCnt++) {
				if(monOptions(msg[msgCnt].text, fileno(logF)) < 0)
					logger(0, "readSMS failed execution of %s", msg[msgCnt].text);
				else {
					logger(3, "readSMS execution of %s OK", msg[msgCnt].text);
					ok++;
				}
			}
		
			// For all of the received message delete them
			for(msgCnt = 0; msg[msgCnt].valid; msgCnt++) {
				sprintf(buf, "AT+CMGD=%d\r", msg[msgCnt].num);
				serialPuts(mod, buf);
				modemOut(mod, buf, sizeof(buf));
				if(strstr(buf, "OK") == NULL)
					logger(0, "readSMS Fallita cancellazione messaggio [%s]\n", msg[msgCnt].text);
			}
		}

		serialClose(mod);
	}
	else if(difftime(time(NULL), errore) > 24*3600) { // error once a day
		logger(1, "readSMS Modem device %s fail open %d-%s", dev, errno, strerror(errno));
		errore = time(NULL);
	}
	logger(3, "readSMS exiting %d", ok);
	return ok;
} 

// checkI2C to see if on battery or power and in case of low battery shutdown
// Batteria 4V86 corrisponde a 13V72 e Power 4V84 corrisponde a 15V35
// Batteria 4V86 corrisponde a ADC 950 e Power 4V84 corrisponde ad ADC 958
// Batteria scarica a 10V=>3V5=>680 quindi shutdown a 650

int checkI2C(int dev)
{
FILE* pf;
unsigned char wr[4], rd[4];
const struct timespec pausa = {0, 40000000};
#define NUM_ADC 2
int i, timing = 21;
const float limits[NUM_ADC*2] = {15.0, 15.5, 13.0, 14.5 }; // 1,2val= power supply, 3,4val=battery
const float factor[NUM_ADC] = {15.35/958.0, 13.72/950.0};
#define MIN_ADC1 (940*15.35/958)
#define MAX_ADC1 (960*15.35/958)
#define MIN_ADC2 (950*13.72/950)
#define MAX_ADC2 (965*13.72/950)
int adc[NUM_ADC];
float  volt[NUM_ADC];
static float oldV[NUM_ADC];
static int conta = -1;
TSFLDS_T data[MAX_TSFLDS];
static bool errore = FALSE;


	logger(3, "checkI2C entering dev=0x%x", dev);
	memset(data, 0, sizeof(TSFLDS_T)*MAX_TSFLDS);
	wr[0] = wr[1] =  wr[2] =  wr[3] = rd[0] = rd[1] =  rd[2] =  rd[3] = 0;
	conta++;

// Batteria 4V86 corrisponde a 13V72 e Power 4V84 corrisponde a 15V35
// Batteria 4V86 corrisponde a ADC 950 e Power 4V84 corrisponde ad ADC 958
	// read ADC1=power supply ADC2=battery level
	for(i=0; i<NUM_ADC; i++) {
		wr[0]=1; //read an ADC
		wr[1]=i+1; 
		if(I2Ccomm(dev, wr, 2, rd, 2) > 0) {
			adc[i] = rd[0]*256+rd[1];
			volt[i] = adc[i]*factor[i];
			if(volt[i] > limits[i*2+1] || volt[i] < limits[i*2]) {
				switch(i) {
				case 0: 
					if(volt[i] < 10.0)
						logger(1, "checkI2C Running on batteries: %2.2fV(ADC=%d)", volt[i], adc[i]);
					else {
						logger(1, "checkI2C Power out of limits: %2.2fV(ADC=%d)", volt[i], adc[i]);
						if(!errore) {
							errore = TRUE;
							nanosleep(&pausa, NULL);
							wr[2] =  wr[3] = rd[0] = rd[1] =  rd[2] =  rd[3] = 0;
						    wr[0] = 3; wr[1] = 4; I2Ccomm(dev, wr, 2, rd, 0);
    						logger(1, "checkI2C Re-Setting Vref to V+: %s", strerror(errno));
						}
					}
					break;
				case 1:
					if(volt[i] < 10.4) { // Minimum battery level otherwise shutdown
						logger(1, "checkI2C Battery low %2.2fV(ADC=%d): shutdown", volt[i], adc[i]);
					}
					else
						logger(1, "checkI2C Battery out of limits: %2.2fV(ADC=%d)", volt[i], adc[i]);
				}
				timing = 5;
			}
			else
				errore = FALSE;
			if(conta%150 || fabs(oldV[i] - volt[i]) > 0.05)
			{
				conta = -1;
				oldV[i] = volt[i];
				data[4+i].st = 2; data[4+i].v.vf = volt[i];
			}
		}
		
		nanosleep(&pausa, NULL);
	}
	if(!thingSpeak(THINGS_CH1_KEY, data))
		logger(3,"checkI2C sent measure to web %s failed", THINGS_CH1_KEY);
	else
		logger(3,"checkI2C sent measure to web %s OK", THINGS_CH1_KEY);

	// dump values in POWFILE file for web page display
	if((pf = fopen(POWFILE, "w")) != NULL) {
		fprintf(pf, "Pow=%2.2fV Batt=%2.2fV\n", volt[0], volt[1]);
		fclose(pf);
	}
	logger(3, "checkI2C exiting timing=%d pow=%2.2f(%d) batt=%2.2f(%d)",
		timing, volt[0], adc[0], volt[1], adc[1]);
	return timing;
}

void *mobileLight() {
#define NREC	18
FILE *pf, *opf, *stpf;
char buf[100], outp[NMACS+1];
char linee[NREC+1][15]; // 10=8 byte per dati ed ora con spazi +1 per \n e +1 per \0
time_t curtime;
struct tm ora;
struct stat sf;
int spegni;
register int i, j, num;

	logger(3, "mobileLight init");
	curtime = time(NULL);
	localtime_r(&curtime, &ora);

	if((pf = popen("sudo arp-scan -l", "r")) == NULL) {
		logger(0, "mobileLight Errore popen(sudo arp-scan) %d-%s", errno, strerror(errno));
		pthread_exit(NULL);
	}
	else if((opf = fopen(MACSDB, "w")) == NULL) {
		logger(0, "mobileLight Errore fopen(%s): %d-%s", MACSDB, errno, strerror(errno));
		pthread_exit(NULL);
	}

	logger(4, "mobileLight Opened %s,%s,%s", "sudo arp-scan pipe", MACSDB, STATFILE);

	memset(outp, '0', NMACS+1);
	while(fgets(buf, sizeof(buf)-1, pf)) {
	char* p1; char* p2;

		if(strstr(buf, "192.168")) {
			p1 = strtok(buf, " \t"); p2 = strtok(NULL, " \t");
			fprintf(opf, "%s,%s\n", p1, p2);
			logger(4, "mobileLight Write: [%s,%s]", p1, p2);
			for(i = 0; i < nmacs; i++) {
				if(strcasecmp(p2, MACS[i]) == 0) {
					outp[i] = '1';
				}
			}
		}
	}
	logger(3, "mobileLight Stato reale cellulari %*.*s", nmacs, nmacs, outp);
	fclose(opf);
	fclose(pf);

	// add one line to DBFILE keepin newer NREC lines
	pf = fopen(DBFILE, "r+");
	if(pf == NULL) {
		pf = fopen(DBFILE, "a+");
		logger(1, "mobileLight fopen(%s, 'a+')", DBFILE);
	}
		
	if(pf) {
		for(num = 0; fgets(buf, sizeof(buf)-1, pf) > 0 && num < NREC; strcpy(linee[num++], buf));
		logger(4, "mobileLight Caricate %d linee", num);

		// scrivo una riga nel database (buttando la piu' vecchia se serve)
		sprintf(linee[num++], DBFMT, nmacs, nmacs, outp, ora.tm_hour, ora.tm_min);
		if(num < NREC) {
			fseek(pf, 0L, SEEK_END);
			fprintf(pf, "%s", linee[num-1]);
			i = num;
		}
		else {
			fseek(pf, 0L, SEEK_SET);
			for(i = 1; i <= NREC; i++)
				fprintf(pf, "%s", linee[i]);
			i = NREC;
		}
		logger(4, "mobileLight Scritte %d linee", i);
		fclose(pf);

		// prendo le ultime tre linee del DB per mitigare i frequenti off dei cellulari
		for(i = num-1; i > num-4 && i > 0; i--) {
			for(j = 0; j < nmacs; j++) {
				if(outp[j] == '0' && linee[i][j] != '0')
					outp[j] = '1';
			}
		}
		if((stpf = fopen(STATFILE, "w")) == NULL) {
			logger(0, "mobileLight Errore fopen(%s): %d-%s", STATFILE, errno, strerror(errno));
			pthread_exit(NULL);
		}
		else {
			fprintf(stpf, "%*.*s\n", nmacs, nmacs, outp);
			fclose(stpf);
			logger(3, "mobileLight Risultato in %*.*s", nmacs, nmacs, outp);
		}
		// Update global presence array for other parts of the software
		for(j = 0; j < nmacs; People[j] = outp[j], j++);
	}
	else {
		logger(0, "mobileLight Errore fopen(%s): %d-%s", DBFILE, errno, strerror(errno));
		pthread_exit(NULL);
	}

	// in case daddy mobile is in house, extend light period
	spegni = (People[0] == '1' || People[1] == '1')?2350:2230;

	// read light status, it is inverted respect normal logic (0=ON, 1=OFF)
    register bool si = MCP23017(MCP23017_PORTA, 1, 5, 0) ? 0 : 1;
	logger(3, "mobileLight mobile ended starting light check light is %s", si?"ON":"OFF");

	// calcolo il mesegiorno
	num = (ora.tm_mon+1)*100+ora.tm_mday;
	for(i = 1; cfg.ol[i].mesgio; i++) {
		if(num < cfg.ol[i].mesgio) {
			i--;
			break;
		}
	}
	if(cfg.ol[i].mesgio == 0) {
		logger(1, "mobileLight cannot find suitable Month-Day in configuration");
		pthread_exit(NULL);
	}

	// calcolo l'ora attuale
	num = ora.tm_hour*100+ora.tm_min;
	
	if(num > cfg.ol[i].ora && num < spegni) {
		if(!si) {
			sprintf(buf, "%sno.flg", LUCEFLAG);
			if(stat(buf, &sf) == 0 && (curtime - sf.st_ctime) < 3600) {
				logger(1, "modemLight: file %s present, cannot turn on light", buf);
				sprintf(buf, "%ssi.flg", LUCEFLAG); // delete any other flag file for on
				unlink(buf);
			}
			else {
				MCP23017(MCP23017_PORTA, 0, 5, 0);
				logger(1, "modemLight Turning ON light");
				unlink(buf);
			}
		}
		else {
			sprintf(buf, "%ssi.flg", LUCEFLAG); // delete any other flag file for on
			unlink(buf);
		}
		pthread_exit(NULL);
	}

	sprintf(buf, "%ssi.flg", LUCEFLAG); // delete any other flag file for on
	if(si) {
		num = stat(buf, &sf);
		if((num == 0 && (curtime - sf.st_ctime) > 3600) || num != 0) {
			unlink(buf);
			MCP23017(MCP23017_PORTA, 0, 5, 1);
			logger(1, "modemLight Turning OFF light");
		}
	}
	else
		unlink(buf);
	sprintf(buf, "%sno.flg", LUCEFLAG);
	unlink(buf);

	logger(3, "mobileLight exiting OK");
	pthread_exit(NULL);
}
